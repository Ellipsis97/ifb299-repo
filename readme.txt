﻿
-------------------- README --------------------
IFB299 Project, Release 2 by Group 29

A live version of this project is available at this ip address: http://107.170.252.110/.

Contained within the application is clearance level checking and redirect functionality. 
In order to log into the staff section of the live web application, under different user 
profiles please use the following details:

	Basic User:
	john@gmail.com

	Parking Inspector:
	markotto@department.com

	Fines Employee/ Manager:
	stevelabour@gmail.com
	
	All Access Testing Account (recommended):
	dickrat56@hotmail.com	

	Password: passkey2016

The bitbucket repository for this project can be found here:
https://bitbucket.org/Ellipsis97/ifb299-repo

Unit testing was implemented using a Google Chrome extension called Ghost Inspector. 
This tool works by recording the inputs the user makes when interacting with the web 
application, the user also flags certain element values for assertion during the recording. 
The tests repeat the actions the user made and look for certain values to be within the 
html, passing if they are there and failing if they are not.
