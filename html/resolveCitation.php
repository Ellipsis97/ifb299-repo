<?PHP
if (empty($_POST))
{
	die();
}

//Flag that this is a parent file.
//Enabling included files to run.
define('CanRun', 1);

//Load external scripts.
require './phpclasses/db.php';
require './phpclasses/session.php';
require './phpclasses/dbhelper.php';
require './phpclasses/utility.php';
require './phpclasses/constants.php';

//Create database and session manager objects.
$db = new DatabaseConnector();
$session = new Session("UserSession");

//Check to see if this session has expired.
if (!$session->IsAuthed())
{
	//Session expired, redirect to the login page.
	$session->HardDelete();
	header("Location: /");
	exit;
}

//Get user details and ensure the session has not been forged...
if (!CheckLogin($db, $session))
{
	//Session invalid, redirect to the login page.
	$session->HardDelete();
	header("Location: /");
	exit;
}

$id = RemoveSymbols($_POST['id']);
$desc = DBSafeText($_POST['desc']);

//Get current date and time.
//Update the permit status for permits that expired today.
$timeZone = new DateTimeZone("Australia/Brisbane");
$currentDate = new DateTime("now", $timeZone);
$currentDate->add(new DateInterval('P1Y'));
$resolutionDate = $currentDate->format('Y-m-d H:i:s');

//Ensure the description will fit in the database.
if (count($desc) > MAX_LENGTH_DESCRIPTION)
{
	print('[{"errorText":"Something went wrong."}]');	
}

$db->Update("citations", ["isResolved", "resolutionDescription", "resolutionDate"], [1, $desc, $resolutionDate], "WHERE `id`='$id'");

$result = $db->Fetch("ifb299.citations", "", "");
if ($result !== false)
{		
	$output = "[";

	while ($row = $result->fetch_assoc())
	{
		//Add comma delimiters, unless this is the first iteration.
		if ($output != "[")
		{
			$output .= ",";
		}
		
		$output .= '{"id":"'.$row["id"].'",';
		$output .= '"firstName":"'.$row["firstName"].'",';
		$output .= '"lastName":"'.$row["lastName"].'",';
		$output .= '"department":"'.$row["department"].'",';
		$output .= '"date":"'.$row["date"].'",';
		$output .= '"time":"'.$row["time"].'",';
		$output .= '"description":"'.$row["description"].'",';
		$output .= '"resolutionDate":"'.$row["resolutionDate"].'",';
		$output .= '"resolutionDescription":"'.$row["resolutionDescription"].'",';
		$output .= '"isResolved":"'.$row["isResolved"].'"}';
	}
	
	$output .= "]";
	print($output);
	
	$result->free();
}
else
{
	print('[{"errorText":"Something went wrong."}]');
}

//We're done with the database connection and result objects
//so now were delete them.
unset($db);
?>