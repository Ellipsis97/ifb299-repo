<?PHP
if (empty($_GET) || $_GET['st'] == -1 ||
	empty($_GET['stable']))
{
	var_dump($_GET);
	die();
}

//Flag that this is a parent file.
//Enabling included files to run.
define('CanRun', 1);

//Load external scripts.
require './phpclasses/db.php';
require './phpclasses/session.php';
require './phpclasses/dbhelper.php';
require './phpclasses/utility.php';

//Create database and session manager objects.
$db = new DatabaseConnector();
$session = new Session("UserSession");

//Check to see if this session has expired.
if (!$session->IsAuthed())
{
	//Session expired, redirect to the login page.
	$session->HardDelete();
	header("Location: /");
	exit;
}

//Get user details and ensure the session has not been forged...
if (!CheckLogin($db, $session))
{
	//Session invalid, redirect to the login page.
	$session->HardDelete();
	header("Location: /");
	exit;
}

//Get search parameters from GET.
$searchType = RemoveSymbols($_GET['st']);
$searchValue = DBSafeText($_GET['sv']);
$searchTable = RemoveSymbols($_GET['stable']);

//Search based on specified table:
switch ($searchTable)
{
	case "permits":
		PermitSearch($db, $searchType, $searchValue);
	break;
	
	case "citations":
		CitationSearch($db, $searchType, $searchValue);
	break;
	
	case "parkingfines":
		ParkingFineSearch($db, $searchType, $searchValue);
	break;
	
	case "smokingfines":
		SmokingFineSearch($db, $searchType, $searchValue);
	break;
	
	default:
		print('[{"errorText":"Something went wrong."}]');
	break;
}

//We're done with the database connection and result objects
//so now were delete them.
unset($db);

//
//Permits:
//
function PermitSearch(&$db, $searchType, $searchValue)
{
	$where = PermitMakeWhere($searchType, $searchValue);

	$result = $db->Fetch("ifb299.permits", "", $where);
	if ($result !== false)
	{	
		$output = "[";

		while ($row = $result->fetch_assoc())
		{
			//Add comma delimiters, unless this is the first iteration.
			if ($output != "[")
			{
				$output .= ",";
			}
			
			$output .= '{"id":"'.$row["id"].'",';
			$output .= '"firstName":"'.$row["firstName"].'",';
			$output .= '"lastName":"'.$row["lastName"].'",';
			$output .= '"dob":"'.$row["dob"].'",';
			$output .= '"department":"'.$row["department"].'",';
			$output .= '"vehicleType":"'.$row["vehicleType"].'",';
			$output .= '"vehicleReg":"'.$row["vehicleReg"].'",';
			$output .= '"startDate":"'.$row["startDate"].'",';
			$output .= '"endDate":"'.$row["endDate"].'",';
			$output .= '"permitStatus":"'.$row["permitStatus"].'",';
			$output .= '"durationType":"'.$row["durationType"].'"}';
		}
		
		$output .= "]";
		print($output);
		
		$result->free();
	}
	else
	{
		print('[{"errorText":"Something went wrong."}]');
	}
}

function PermitMakeWhere($searchType, $searchValue)
{
	$result = "WHERE ";
	
	switch ($searchType)
	{
		case 1:
			$result .= 'id = "'.$searchValue.'"';
		break;
		
		case 2:
			$result .= 'firstName LIKE "%'.$searchValue.'%"';
		break;
		
		case 3:
			$result .= 'lastName LIKE "%'.$searchValue.'%"';
		break;
		
		case 4:
			$result .= 'dob = "'.$searchValue.'"';
		break;
		
		case 5:
			$result .= 'department LIKE "%'.$searchValue.'%"';
		break;
		
		case 6:
			$result .= 'vehicleType LIKE "%'.$searchValue.'%"';
		break;
		
		case 7:
			$result .= 'vehicleReg LIKE "%'.$searchValue.'%"';
		break;
		
		case 8:
			$result .= 'startDate = "'.preg_replace('/[^A-Za-z0-9_\/]/', '', $searchValue).'"';
		break;
		
		case 9:
			$result .= 'endDate = "'.preg_replace('/[^A-Za-z0-9_\/]/', '', $searchValue).'"';
		break;
		
		case 10:
			$result .= 'durationType = "'.$searchValue.'"';
		break;
		
		default:
		break;
	}
	
	return $result;
}

//
//Citations:
//

function CitationSearch(&$db, $searchType, $searchValue)
{	
	$where = CitationMakeWhere($searchType, $searchValue);

	$result = $db->Fetch("ifb299.citations", "", $where);
	if ($result !== false)
	{		
		$output = "[";

		while ($row = $result->fetch_assoc())
		{
			//Add comma delimiters, unless this is the first iteration.
			if ($output != "[")
			{
				$output .= ",";
			}
			
			$output .= '{"id":"'.$row["id"].'",';
			$output .= '"firstName":"'.$row["firstName"].'",';
			$output .= '"lastName":"'.$row["lastName"].'",';		
			$output .= '"department":"'.$row["department"].'",';
			$output .= '"date":"'.$row["date"].'",';
			$output .= '"time":"'.$row["time"].'",';
			$output .= '"description":"'.$row["description"].'",';
			$output .= '"resolutionDate":"'.$row["resolutionDate"].'",';
			$output .= '"resolutionDescription":"'.$row["resolutionDescription"].'",';
			$output .= '"isResolved":"'.$row["isResolved"].'"}';
		}
		
		$output .= "]";
		print($output);
		
		$result->free();
	}
	else
	{
		print('[{"errorText":"Something went wrong."}]');
	}
}

function CitationMakeWhere($searchType, $searchValue)
{
	$result = "WHERE ";
	
	switch ($searchType)
	{
		case 1:
			$result .= 'id = "'.$searchValue.'"';
		break;
		
		case 2:
			$result .= 'firstName LIKE "%'.$searchValue.'%"';
		break;
		
		case 3:
			$result .= 'lastName LIKE "%'.$searchValue.'%"';
		break;
		
		case 4:
			$result .= 'department LIKE "%'.$searchValue.'%"';
		break;
		
		case 5:
			$result .= 'date = "'.$searchValue.'"';
		break;
		
		case 6:
			$result .= 'time = "'.$searchValue.'"';
		break;
		
		case 7:
			$result .= 'description LIKE "%'.$searchValue.'%"';
		break;
		
		case 8:
			$result .= 'resolutionDate = "'.preg_replace('/[^A-Za-z0-9_\/]/', '', $searchValue).'"';
		break;
		
		case 9:
			$result .= 'resolutionDescription LIKE "%'.$searchValue.'%"';
		break;
		
		case 10:
			$result .= 'isResolved = "'.$searchValue.'"';
		break;
	}

	return $result;
}

//
//Parking Fines:
//
function ParkingFineSearch(&$db, $searchType, $searchValue)
{	
	$where = ParkingFineMakeWhere($searchType, $searchValue);

	$result = $db->Fetch("ifb299.parkingfines", "", $where);
	if ($result !== false)
	{		
		$output = "[";

		while ($row = $result->fetch_assoc())
		{
			//Add comma delimiters, unless this is the first iteration.
			if ($output != "[")
			{
				$output .= ",";
			}
			
			$output .= '{"id":"'.$row["id"].'",';
			$output .= '"date":"'.$row["date"].'",';
			$output .= '"time":"'.$row["time"].'",';		
			$output .= '"vehicleReg":"'.$row["vehicleReg"].'",';
			$output .= '"vehicleType":"'.$row["vehicleType"].'",';
			$output .= '"paid":"'.$row["paid"].'"}';
		}
		
		$output .= "]";
		print($output);
		
		$result->free();
	}
	else
	{
		print('[{"errorText":"Something went wrong."}]');
	}
}

function ParkingFineMakeWhere($searchType, $searchValue)
{
	$result = "WHERE ";
	
	switch ($searchType)
	{
		case 1:
			$result .= 'id = "'.$searchValue.'"';
		break;
		
		case 2:
			$result .= 'date = "'.$searchValue.'"';
		break;
		
		case 3:
			$result .= 'time = "'.$searchValue.'"';
		break;
		
		case 4:
			$result .= 'vehicleReg LIKE "%'.$searchValue.'%"';
		break;
		
		case 5:
			$result .= 'vehicleType LIKE "%'.$searchValue.'%"';
		break;
		
		case 6:		
			$result .= 'paid = "'.$searchValue.'"';
		break;
	}
	
	return $result;
}

//
//Smoking Fines:
//
function SmokingFineSearch(&$db, $searchType, $searchValue)
{	
	$where = SmokingFineMakeWhere($searchType, $searchValue);

	$result = $db->Fetch("ifb299.smokingfines", "", $where);
	if ($result !== false)
	{		
		$output = "[";

		while ($row = $result->fetch_assoc())
		{
			//Add comma delimiters, unless this is the first iteration.
			if ($output != "[")
			{
				$output .= ",";
			}
			
			$output .= '{"id":"'.$row["id"].'",';
			$output .= '"firstName":"'.$row["firstName"].'",';
			$output .= '"lastName":"'.$row["lastName"].'",';		
			$output .= '"department":"'.$row["department"].'",';
			$output .= '"supervisor":"'.$row["supervisor"].'",';
			$output .= '"date":"'.$row["date"].'",';
			$output .= '"time":"'.$row["time"].'",';
			$output .= '"location":"'.$row["location"].'"}';
		}
		
		$output .= "]";
		print($output);
		
		$result->free();
	}
	else
	{
		print('[{"errorText":"Something went wrong."}]');
	}
}

function SmokingFineMakeWhere($searchType, $searchValue)
{
	$result = "WHERE ";
	
	switch ($searchType)
	{
		case 1:
			$result .= 'id = "'.$searchValue.'"';
		break;
		
		case 2:
			$result .= 'firstName LIKE "%'.$searchValue.'%"';
		break;
		
		case 3:
			$result .= 'lastName LIKE "%'.$searchValue.'%"';
		break;
		
		case 4:
			$result .= 'department LIKE "%'.$searchValue.'%"';
		break;
		
		case 5:
			$result .= 'supervisor LIKE "%'.$searchValue.'%"';
		break;
		
		case 6:
			$result .= 'date = "'.$searchValue.'"';
		break;
		
		case 7:
			$result .= 'time = "'.$searchValue.'"';
		break;
		
		case 8:
			$result .= 'location LIKE "%'.$searchValue.'%"';
		break;
	}
	
	return $result;
}

?>