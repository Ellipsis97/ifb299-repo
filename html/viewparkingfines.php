<?PHP
//Flag that this is a parent file.
//Enabling included files to run.
define('CanRun', 1);

//Load external scripts.
require './phpclasses/db.php';
require './phpclasses/session.php';
require './phpclasses/dbhelper.php';
require './phpclasses/navbar.php';
require './phpclasses/constants.php';

//Create database and session manager objects.
$db = new DatabaseConnector();
$session = new Session("UserSession");

//Check to see if this session has expired.
if (!$session->IsAuthed())
{
	//Session expired, redirect to the login page.
	$session->HardDelete();
	header("Location: /");
	exit;
}

//Get user details and ensure the session has not been forged...
if (!CheckLogin($db, $session))
{
	//Session invalid, redirect to the login page.
	$session->HardDelete();
	header("Location: /");
	exit;
}

//Get user personal details so we can customise page contents.
$userID = $session->Get("user-id");
$userDetails = GetUserDetails($db, $userID);

//Redirect if we don't have clearance.
if ($userDetails['clearance'] < 3)
{
	header("Location: /403.php");
	exit;
}


$result = $db->Fetch("ifb299.parkingfines", "", "");
if ($result !== false)
{	
	$tableStart = "<table class=\"table\"><tbody>";
	
	//Table headings:
	$unpaidTableHeading = AddHeadingsToTable(true);
	$paidTableHeading = AddHeadingsToTable();
	
	$tableEnd = "</tbody></table>";
	
	//Declare each table.
	$unpaidTableOutput = "";
	$paidTableOutput = "";
	
	//Get current date and time.
	$timeZone = new DateTimeZone("Australia/Brisbane");
	$currentDate = new DateTime("now", $timeZone);
	
	while ($row = $result->fetch_assoc())
	{
		if ($row['paid'] == 1)
		{
			AddRowToTable($paidTableOutput, $row, $currentDate);
		}
		else
		{
			AddRowToTable($unpaidTableOutput, $row, $currentDate, true);
		}
	}
	
	$result->free();
}

//We're done with the database connection and result objects
//so now were delete them.
unset($db);

function AddHeadingsToTable($unpaid = false)
{
	$result = "<tr><th>ID</th>";	
	$result .= "<th>Issued Date</th>";
	$result .= "<th>Issued Time</th>";
	$result .= "<th>Vehicle Registration</th>";
	
	if ($unpaid)
	{
		$result .= "<th>Vehicle Type</th>";
		$result .= "<th>Confirm</th></tr>";
	}
	else
	{
		$result .= "<th>Vehicle Type</th></tr>";
	}
	
	return $result;
}

function AddRowToTable(&$outputString, $row, $currentDate, $unpaid = false)
{	
	$id = $row['id'];
	$date = $row['date'];
	$time = $row['time'];
	$vehicleReg = $row['vehicleReg'];
	$vehicleType = $row['vehicleType'];
	
	//Convert issued date value to DateTime object.
	//Get the difference in days from now to when
	//this fine was issued.
	$issuedDate = new DateTime($date.$time);
	$interval = $currentDate->diff($issuedDate);
	$daysPassed = $interval->format('%R%a');
	
	//If a certain number of days has passed, this fine is overdue.
	if ($unpaid && $daysPassed < -OVERDUE_FINE_THRESHOLD)
	{
		$outputString .= "<tr class=\"danger\"><td>".$id."</td>";
	}
	else
	{
		$outputString .= "<tr><td>".$id."</td>";		
	}	
	
	$outputString .= "<td>".$date."</td>";
	$outputString .= "<td>".$time."</td>";
	$outputString .= "<td>".$vehicleReg."</td>";
	$outputString .= "<td>".$vehicleType."</td>";
	
	if ($unpaid)
	{
		$outputString .= "<td><button class=\"btn btn-default\" onclick=\"ConfirmPayment($id)\"><span class=\"glyphicon glyphicon-ok\"></span></button>";
	}
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="" />
	<meta name="author" content="" />
	<link rel="icon" type="image/png" href="images/favicon.ico" />
	<title>PH&S: View Parking Fines</title>
	<!-- Bootstrap core scripts -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<!-- Page specific scripts -->
	<script src="js/navbar.js"></script>
	<script src="js/viewfines.js"></script>
	<!-- Bootstrap core CSS -->
	<link href="/css/bootstrap.min.css" rel="stylesheet" />
	<!-- Page specific CSS -->
	<link href="/css/sidebar.css" rel="stylesheet" />
	<link href="/css/viewpages.css" rel="stylesheet" />
</head>
<body>
	<div id="wrapper">
		<?PHP PrintNavBar($userDetails); ?>
		<div id="page-content-wrapper" class="container-fluid">
			<div class="container">
				<!-- Navigation Toggle Button -->
				<a href="#menu-toggle" class="btn btn-default" id="menu-toggle"><span class="glyphicon glyphicon-menu-hamburger"></span><span> Hide Navigation</span></a>
				<!-- Jumbotron -->
				<div class="jumbotron">
					<h1>Parking Fine Search</h1>
					<p>Please enter search options below...</p>
					<!-- Fines Search Form -->
					<form id="search-form" class="form-horizontal">
						<div class="form-group">
							<!-- Search Input and Button -->
							<div class="col-sm-10">
								<div class="input-group">
									<label class="sr-only" for="search-value">Search Value</label>
									<input type="text" class="form-control" aria-label="Text input" id="search-value" name="search-value" placeholder="Enter search value and press enter to submit...">
									<div class="input-group-btn" role="group" aria-label="...">
										<button type="submit" class="btn btn-default" id="submit-button">Search</button>
									</div>
								</div>
							</div>
							<!-- Search By Type -->
							<div class="col-sm-2">
								<label class="sr-only" for="search-type">Search Type</label>
								<select class="col-sm-2 form-control" id="search-type" name="search-type">
									<option value="-1">Search By...</option>
									<option value="1">ID</option>
									<option value="2">Date</option>
									<option value="3">Time</option>
									<option value="4">Vehicle Registration</option>
									<option value="5">Vehicle Type</option>
									<option value="6">Paid</option>
								</select>
							</div>
						</div>
						<!-- Table Error -->
						<div class="form-group" id="search-table-error">
							<div class="col-sm-12">
								<div class="alert alert-warning" role="alert"><span><b>Error: </b></span><span></span></div>
							</div>
						</div>
						<!-- Result Table -->
						<div class="form-group" id="search-table-section">
							<div class="col-sm-12">
								<table class="table" id="search-table"></table>
							</div>
						</div>
					</form>
				</div>
				<p><span style="color: red">* </span><b>Fines more than <?PHP print(OVERDUE_FINE_THRESHOLD); ?> days overdue are highlighted in red.</b></p>
				<!-- Fines Tables -->
				<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
					<!-- Unpaid Fines -->
					<div class="panel panel-default panel-warning">
						<div class="panel-heading" role="tab" id="headingOne">
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse" data-parent="#accordion" 
								href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
									Unpaid Fines
								</a>
							</h4>
						</div>
						<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
							<div class="panel-body" id="unpaid-table-section">
								<?PHP print($tableStart.$unpaidTableHeading.$unpaidTableOutput.$tableEnd); ?>
							</div>
						</div>
					</div>
					<!-- Paid Fines -->
					<div class="panel panel-default panel-success">
						<div class="panel-heading" role="tab" id="headingTwo">
							<h4 class="panel-title">
								<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" 
								href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
									Paid Fines
								</a>
							</h4> 
						</div>
						<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
							<div class="panel-body" id="paid-table-section">
								<?PHP print($tableStart.$paidTableHeading.$paidTableOutput.$tableEnd); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Logout Confirmation Modal -->
	<?PHP PrintLogOutModal(); ?>
</body>
</html>