<?PHP
//Flag that this is a parent file.
//Enabling included files to run.
define('CanRun', 1);

//Load external scripts.
require './phpclasses/db.php';
require './phpclasses/session.php';
require './phpclasses/dbhelper.php';
require './phpclasses/navbar.php';

//Create database and session manager objects.
$db = new DatabaseConnector();
$session = new Session("UserSession");

//Check to see if this session has expired.
if (!$session->IsAuthed())
{
	//Session expired, redirect to the login page.
	$session->HardDelete();
	header("Location: /");
	exit;
}

//Get user details and ensure the session has not been forged...
if (!CheckLogin($db, $session))
{
	//Session invalid, redirect to the login page.
	$session->HardDelete();
	header("Location: /");
	exit;
}

//Get user personal details so we can customise page contents.
$userID = $session->Get("user-id");
$userDetails = GetUserDetails($db, $userID);

//Redirect if we don't have clearance.
if ($userDetails['clearance'] < 2)
{
	header("Location: /403.php");
	exit;
}

//Get current date and time.
//Update the permit status for permits that expired today.
$timeZone = new DateTimeZone("Australia/Brisbane");
$currentDate = new DateTime("now", $timeZone);
$currentDate = $currentDate->format('Y-m-d H:i:s');
$db->Update("permits", ['permitStatus'], ['Expired'], "WHERE endDate < '$currentDate'");

$result = $db->Fetch("ifb299.permits", "", "");
if ($result !== false)
{	
	$tableStart = "<table class=\"table\"><tbody>";
	
	//Table headings:
	$tableHeading .= AddHeadingsToTable();
	$pendingTableHeading .= AddHeadingsToTable(true);
	
	$tableEnd = "</tbody></table>";
	
	//Declare each table.
	$searchTableOutput = "";
	$pendingTableOutput = "";
	$currentTableOutput = "";
	$expiredTableOutput = "";
	
	while ($row = $result->fetch_assoc())
	{
		$permitStatus = $row['permitStatus'];
		
		switch ($permitStatus)
		{
			case "Pending":
				AddRowToTable($pendingTableOutput, $row, true);
			break;
			
			case "Current":
				AddRowToTable($currentTableOutput, $row);
			break;
			
			case "Expired":
				AddRowToTable($expiredTableOutput, $row);
			break;
		}
	}
	
	$result->free();
}

//We're done with the database connection and result objects
//so now were delete them.
unset($db);

function AddHeadingsToTable($approveCol = false)
{
	$result = "<tr><th>ID</th>";
	$result .= "<th>First Name</th>";
	$result .= "<th>Last Name</th>";
	$result .= "<th>Date of Birth</th>";
	$result .= "<th>Department</th>";
	$result .= "<th>Vehicle Type</th>";
	$result .= "<th>Vehicle Registration</th>";
	$result .= "<th>Start Date</th>";
	$result .= "<th>End Date</th>";
	
	if (!$approveCol)
	{
		$result .= "<th>Duration Type</th></tr>";
	}
	else
	{
		$result .= "<th>Duration Type</th>";
		$result .= "<th>Approve</th></tr>";
	}
	
	return $result;
}

function AddRowToTable(&$outputString, $row, $approveCol = false)
{
	$id = $row['id'];
	$firstName = $row['firstName'];
	$lastName = $row['lastName'];
	$dob = $row['dob'];
	$department = $row['department'];
	$vehicleType = $row['vehicleType'];
	$vehicleReg = $row['vehicleReg'];
	$startDate = $row['startDate'];
	$endDate = $row['endDate'];
	$durationType = $row['durationType'];

	$outputString .= "<tr><td>".$id."</td>";
	$outputString .= "<td>".$firstName."</td>";
	$outputString .= "<td>".$lastName."</td>";
	$outputString .= "<td>".$dob."</td>";
	$outputString .= "<td>".$department."</td>";
	$outputString .= "<td>".$vehicleType."</td>";
	$outputString .= "<td>".$vehicleReg."</td>";
	$outputString .= "<td>".$startDate."</td>";
	$outputString .= "<td>".$endDate."</td>";
	$outputString .= "<td>".$durationType."</td>";
	
	if ($approveCol)
	{
		$outputString .= "<td><button class=\"btn btn-default\" onclick=\"Approve($id)\"><span class=\"glyphicon glyphicon-ok\"></span></button>";
		$outputString .= "<button class=\"btn btn-default\" onclick=\"Remove($id)\"><span class=\"glyphicon glyphicon-remove\"></span></button></td>";
	}
	$outputString .= "</tr>";
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="" />
	<meta name="author" content="" />
	<link rel="icon" type="image/png" href="images/favicon.ico" />
	<title>PH&S: View Permits</title>
	<!-- Bootstrap core scripts -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<!-- Page specific scripts -->
	<script src="js/navbar.js"></script>
	<script src="js/viewpermits.js"></script>
	<!-- Bootstrap core CSS -->
	<link href="/css/bootstrap.min.css" rel="stylesheet" />
	<!-- Page specific CSS -->
	<link href="/css/sidebar.css" rel="stylesheet" />
	<link href="/css/viewpages.css" rel="stylesheet" />
</head>
<body>
	<div id="wrapper">
		<?PHP PrintNavBar($userDetails); ?>
		<div id="page-content-wrapper" class="container-fluid">
			<div class="container">
				<!-- Navigation Toggle Button -->
				<a href="#menu-toggle" class="btn btn-default" id="menu-toggle"><span class="glyphicon glyphicon-menu-hamburger"></span><span> Hide Navigation</span></a>
				<!-- Jumbotron -->
				<div class="jumbotron">
					<h1>Permit Search</h1>
					<p>Please enter search options below...</p>
					<!-- Permit Search Form -->
					<form id="search-form" class="form-horizontal">
						<div class="form-group">
							<!-- Search Input and Button -->
							<div class="col-sm-10">
								<div class="input-group">
									<label class="sr-only" for="search-value">Search Value</label>
									<input type="text" class="form-control" aria-label="Text input" id="search-value" name="search-value" placeholder="Enter search value and press enter to submit...">
									<div class="input-group-btn" role="group" aria-label="...">
										<button type="submit" class="btn btn-default" id="submit-button">Search</button>
									</div>
								</div>
							</div>
							<!-- Search By Type -->
							<div class="col-sm-2">
								<label class="sr-only" for="search-type">Search Type</label>
								<select class="col-sm-2 form-control" id="search-type" name="search-type">
									<option value="-1">Search By...</option>
									<option value="1">Permit ID</option>
									<option value="2">First Name</option>
									<option value="3">Last Name</option>
									<option value="4">Date of Birth</option>
									<option value="5">Department</option>
									<option value="6">Vehicle Type</option>
									<option value="7">Vehicle Registration</option>
									<option value="8">Issue Date</option>
									<option value="9">Expiry Date</option>
									<option value="10">Duration Type</option>
								</select>
							</div>
						</div>
						<!-- Table Error -->
						<div class="form-group" id="search-table-error">
							<div class="col-sm-12">
								<div class="alert alert-warning" role="alert"><span><b>Error: </b></span><span></span></div>
							</div>
						</div>
						<!-- Result Table -->
						<div class="form-group" id="search-table-section">
							<div class="col-sm-12">
								<table class="table" id="search-table"></table>
							</div>
						</div>
					</form>
				</div>
				<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
					<!-- Pending Permits -->
					<?PHP if ($userDetails['clearance'] >= 3): ?>
					<div class="panel panel-default panel-info">
						<div class="panel-heading" role="tab" id="headingOne">
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse" data-parent="#accordion" 
								href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
									Pending Permits
								</a>
							</h4>
						</div>
						<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
							<div class="panel-body" id="pending-table-section">
								<?PHP print($tableStart.$pendingTableHeading.$pendingTableOutput.$tableEnd); ?>
							</div>
						</div>
					</div>
					<?PHP endif ?>
					<!-- Current Permits -->
					<div class="panel panel-default panel-success">
						<div class="panel-heading" role="tab" id="headingTwo">
							<h4 class="panel-title">
								<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" 
								href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
									Current Permits
								</a>
							</h4> 
						</div>
						<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
							<div class="panel-body" id="current-table-section">
								<?PHP print($tableStart.$tableHeading.$currentTableOutput.$tableEnd); ?>
							</div>
						</div>
					</div>
					<!-- Expired Permits -->
					<?PHP if ($userDetails['clearance'] >= 3): ?>
					<div class="panel panel-default panel-warning">
						<div class="panel-heading" role="tab" id="headingThree">
							<h4 class="panel-title">
								<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" 
								href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
									Expired Permits
								</a>
							</h4>
						</div>
						<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
							<div class="panel-body" id="expired-table-section">
								<?PHP print($tableStart.$tableHeading.$expiredTableOutput.$tableEnd); ?>
							</div>
						</div>
					</div>
					<?PHP endif; ?>
				</div>
			</div>
		</div>
	</div>
	<!-- Logout Confirmation Modal -->
	<?PHP PrintLogOutModal(); ?>
</body>
</html>