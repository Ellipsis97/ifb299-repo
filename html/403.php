<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="" />
	<meta name="author" content="" />
	<link rel="icon" type="image/png" href="images/favicon.ico" />
	<title>403 Error</title>
	<!-- Bootstrap core scripts -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<!-- Bootstrap core CSS -->
	<link href="/css/bootstrap.min.css" rel="stylesheet" />
</head>
<body>
	<div class="container">
		<div class="jumbotron" style="margin-top: 50px;">
			<h1>403 - Permission Denied</h1>
			<p>You do not have permission to retrieve the URL or link you requested. Please make sure you
			have been approved clearance before attempting access once more.</p>
			<p>Tip: You can check what features you have access to via Virtual.</p>
			<a type="button" class="btn btn-lg btn-default" href="virtual.php"><span class="glyphicon glyphicon-arrow-left"></span> Return to Virtual</a>
		</div>
	</div>
</body>
</html>