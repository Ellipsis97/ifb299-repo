<?PHP
//Disallow direct access.
if (!defined('CanRun'))
{
	die();
}

define("MAX_LENGTH_NAME", 32);
define("MAX_LENGTH_DATE_STRING", 10);
define("MAX_LENGTH_TIME_STRING", 5);
define("MAX_LENGTH_DEPARTMENT", 64);
define("MAX_LENGTH_VEHICLE_REG", 6);
define("MAX_LENGTH_VEHICLE_TYPE", 7);
define("MAX_LENGTH_LOCATION", 64);
define("MAX_LENGTH_DESCRIPTION", 255);

//
//Login:
//
define("MAX_LENGTH_LOGIN_EMAIL", 32);
define("MAX_LENGTH_LOGIN_PASS", 32);

define("OVERDUE_FINE_THRESHOLD", 7);

?>