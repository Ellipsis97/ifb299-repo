<?PHP

function PrintNavBar(&$userDetails)
{
	$firstName = $userDetails['firstName'];
	$lastName = $userDetails['lastName'];
	$picture = $userDetails['picture'];
	$clearance = $userDetails['clearance'];
	
	print("<!-- Navigation Bar -->
		<div id=\"sidebar-wrapper\">
			<ul class=\"sidebar-nav\">
				<!-- User Profile Picture -->
				<div style=\"width:50%;margin:20px auto 0px\"> <img src=\"./images/$picture\" class=\"img-thumbnail\" alt=\"thumbnail\" align=\"middle\" /></div>
				<!-- Links -->
				<li class=\"sidebar-brand\">$firstName $lastName</li>
				<li><a href=\"/virtual.php\">Virtual</a></li>
				<li><a href=\"/permitapplication.php\">Parking Permit Application</a></li>
				<li><a href=\"/issuecitation.php\">Issue Citation</a></li>"
	);
	
	switch($clearance)
	{
		case 2: //Parking Inspector
			print("<li><a href=\"/issueparkingfine.php\">Issue Parking Fine</a></li>	
				<li><a href=\"/issuesmokingfine.php\">Issue Smoking Fine</a></li>
				<li><a href=\"/viewpermits.php\">View Permits</a></li>");
		break;
		case 3: //Fines Employee
			print("<li><a href=\"/viewpermits.php\">View Permits</a></li>
				<li><a href=\"/viewcitations.php\">View Citations</a></li>
				<li><a href=\"/viewparkingfines.php\">View Parking Fines</a></li>
				<li><a href=\"/viewsmokingfines.php\">View Smoking Fines</a></li>");
		break;
		case 4: //Testing
			print("<li><a href=\"/issueparkingfine.php\">Issue Parking Fine</a></li>	
				<li><a href=\"/issuesmokingfine.php\">Issue Smoking Fine</a></li>
				<li><a href=\"/viewpermits.php\">View Permits</a></li>
				<li><a href=\"/viewcitations.php\">View Citations</a></li>
				<li><a href=\"/viewparkingfines.php\">View Parking Fines</a></li>
				<li><a href=\"/viewsmokingfines.php\">View Smoking Fines</a></li>");
		break;
	}
	
	print("<li><a href=\"#\" data-toggle=\"modal\" data-target=\"#myModal\">Logout</a></li></ul></div>");
}

function PrintLogOutModal()
{
	print ("<div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\">
		<div class=\"modal-dialog\" role=\"document\">
			<div class=\"modal-content\">
				<div class=\"modal-header\">
					<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
					<h4 class=\"modal-title\" id=\"myModalLabel\">Are you sure you want to logout?</h4>
				</div>
				<div class=\"modal-footer\">
					<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancel</button>
					<a href=\"/logout.php\" class=\"btn btn-primary\">Logout</a>
				</div>
			</div>
		</div>
	</div>");
}
?>