<?PHP

//Disallow direct access.
if (!defined("CanRun"))
{
	header("Location: 404.html");
}

class DatabaseConnector
{
	//Properties.
	private $schemaName;
	private $enableOutput;
	private $link;

	function __construct()
	{	
		$this->enableOutput = false;
	
		//Test server values.
		$hostName = "localhost";
		$username = "IFB299Group";
		$password = "passkey2016";
		$this->schemaName = "ifb299";
	
		//Production server values.
		//$hostName = "server address here";
		//$username = "username here";
		//$password = "password here";
		//$this->schemaName = "schema";
	
		//Connect to db.
		$this->link = new mysqli($hostName, $username, $password, $this->schemaName);

		//If we can't connect tell the user.
		if (!$this->link)
		{
			die($this->link->error());
		}
	}
	
	function __destruct()
	{
		$this->link->close();
	}
	
	public function Fetch($table, $select = "", $where = "")
	{	
		//Set where to null if not specified.
		$where = $this->HandleEmpty($where, 'WHERE');
		
		//Set select to * if not specified.
		$select = $this->HandleEmpty($select, 'SELECT');
		
		$query = "SELECT $select FROM $table $where";
		$result = $this->ExecuteQuery($query);
		
		return $result;
	}
	
	public function Update($table, $fieldArray, $valueArray, $where)
	{
		//TODO: There is a bug where this is the only method that uses the schema name.
		$query = "UPDATE `".$this->schemaName."`.`$table` SET ";
		
		for ($i = 0; $i < count($fieldArray) - 1; $i++)
		{
			$query = $query . " `".$fieldArray[$i]."`='".$valueArray[$i]."',";
		}
		
		$query = $query . " `".$fieldArray[count($fieldArray) - 1]."`='".$valueArray[count($fieldArray) - 1]."' ";
		$where = $this->HandleEmpty($where, "WHERE");
		$query = $query.$where;
		
		$this->ExecuteQuery($query);
	}
	
	public function Insert($table, $fieldArray, $valueArray)
	{
		$query = "INSERT INTO `".$this->schemaName."`.`$table` (";
		
		for ($i = 0; $i < count($fieldArray) - 1; $i++)
		{
			$query = $query . "`".$fieldArray[$i]."`, ";
		}
		
		$query = $query . " `".$fieldArray[count($fieldArray) - 1]."`) ";
		$query = $query . "VALUES (";
		
		for ($i = 0; $i < count($valueArray) - 1; $i++)
		{
			$query = $query . "'".$valueArray[$i]."', ";
		}
		
		$query = $query ."'".$valueArray[count($valueArray) - 1]."')";
		
		$this->ExecuteQuery($query);
	}
	
	public function Delete($table, $id)
	{
		$query = "DELETE FROM `".$this->schemaName."`.`$table` WHERE `id`='$id'";
		
		$this->ExecuteQuery($query);
	}
	
	private function HandleEmpty($query, $type)
	{	
		if (empty($query))
		{
			switch ($type)
			{
				case 'WHERE':
				return "";
				
				case 'SELECT':
				return " * ";
				
				case 'UPDATE':
				die("Must specify what to update.");
				
				default:
				die("Must specify a valid handleEmpty type.");
			}
		}
		else
		{
			return $query;
		}
	}
	
	private function ExecuteQuery($query)
	{
		//Uncomment to debug.
		//echo $query;
	
		if (!$query)
		{
			die("Empty query.");
		}
		
		$result = $this->link->query($query);	
		
		if ($this->enableOutput && $this->link->error)
		{
			//Do error logging.
			//Send the admin an email?
			print($this->link->error);
		}
		
		return $result;
	}
}
?>