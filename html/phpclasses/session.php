<?PHP
if (!defined('CanRun'))
{
	//header("Location: /other/404.html");
	die();
}

class Session
{
	function __construct($Name)
	{		
		session_set_cookie_params(0, '/', '', false , true);
		session_name($Name);
		session_start();
		
		if (!isset($_SESSION["token"]))
		{
			$_SESSION["token"] = $this->ChallengeToken();
		}
		
		//var_dump($_SESSION);
	}
	
	public function Assign($VarName, $Value)
	{	
		$_SESSION[$VarName] = $Value;
	}
	
	public function Get($VarName)
	{
		if (isset($_SESSION[$VarName]))
		{
			return $_SESSION[$VarName];
		}
		else
		{
			return null;
		}		
	}
	
	public function Dump()
	{
		$Params = session_get_cookie_params();
		
		echo "<b>SESSION VALUES:</b><br>"
			."<b>Lifetime: </b>".$Params['lifetime']."<br>"
			."<b>Info Path: </b>".$Params['path']."<br>"
			."<b>Domain: </b>".$Params['domain']."<br>"
			."<b>Is Secure?: </b>".$Params['secure']."<br>"
			."<b>HTTP Only?: </b>".$Params['httponly']."<br>";
	}
	
	public function SoftDelete()
	{
		//Just empty session data.
		$_SESSION = array();		
	}
	
	public function HardDelete()
	{		
		//Setting the expiration date to the past on the cookie causes deletion.
		//In this case we move it back about a year ago.
		$Params = session_get_cookie_params();
		setcookie("user_log", '', time() - 1314000,
			$Params['path'], $Params['domain'],
			$Params['secure'], $Params['httponly']);
			
		session_regenerate_id();
		session_destroy();
	}
	
	public function ChallengeToken()
	{
		return strrev(md5(session_id() + time()));
	}
	
	public function TokensMatch($Value)
	{
		return ($this->Get("token") == $Value) ? true : false;
	}
	
	public function IsAuthed()
	{
		return ($this->Get("token") == null || $this->IsExpired() == true) ? true : false;
	}
	
	private function IsExpired()
	{
		//printf($this->Get("expire-time") - time());
		return (time() < $this->Get("expire-time")) ? true : false;
	}
}
?>