<?PHP

//Ensures the session has not been forged...
function CheckLogin($db, $session)
{
	$userID = $session->Get("user-id");
	$result = $db->Fetch("ifb299.users", "email", "WHERE id=".$userID);
	if ($result !== false)
	{
		//Get row from database table and free the result object.
		$row = $result->fetch_assoc();
		$result->free();
		
		//Ensure the hash stored in the session object is valid
		//by creating the hash again using the details and seeing if they match.
		if (sha1($userID."oldboy".$row['email']) != $session->Get("user-hash"))
		{		
			//Session invalid, redirect to the login page.
			return false;
		}
	}
	else
	{
		//User ID in session object is invalid,
		//redirect to the login page.
		return false;
	}
	
	return true;
}

//Get user personal details so we can customise page contents.
function GetUserDetails($db, $userID)
{
	$userDetails = [];

	$result = $db->Fetch("ifb299.users", "firstName, lastName, email, dob, department, picture, clearance", "WHERE id=".$userID);
	if ($result !== false)
	{
		//Get row from database table and free the result object.
		$row = $result->fetch_assoc();
		$result->free();
		
		$userDetails['firstName'] = $row['firstName'];
		$userDetails['lastName'] = $row['lastName'];
		$userDetails['email'] = $row['email'];$userDetails['picture'] = $row['picture'];
		$userDetails['dob'] = $row['dob'];
		$userDetails['department'] = $row['department'];
		$userDetails['clearance'] = $row['clearance'];
	}
	else
	{
		//Database error.
		exit;
	}
	
	return $userDetails;
}

?>