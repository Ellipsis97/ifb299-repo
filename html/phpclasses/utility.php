<?PHP

//Disallow direct access.
if (!defined('CanRun'))
{
	//header("Location: /other/404.html");
	die();
}

//Generate a password hash.
function GetPass($username, $password)
{
	//Uppercase username so the username is not case sensitive.
	$username = mb_strtoupper($username);

	$value = hash("whirlpool", bin2hex(strrev(stripslashes($username)))."inceptionwasgood".md5(stripslashes(strtoupper($password)), false), false);
	$value = hash("whirlpool", $value, false);
	
	return $value;
}

//Remove any symbols from a given string.
function RemoveSymbols($text)
{
	$result = "";
	$result = preg_replace('/[^A-Za-z0-9_:\/ ]/', '', $text);
	
	return $result;
}

//Returns true if there are symbols in a given string.
//Returns false if there is not.
function HasSymbols($text)
{
	if (preg_match('/[^A-Za-z ]/', $text) === 1)
	{
		return true;
	}
	
	return false;
}

//Convert text to html entities, including both double and single quotes.
function DBSafeText($text, $removeSymbols = false)
{
	$result = "";	
	$result = htmlentities($text, ENT_QUOTES);
	
	if ($removeSymbols)
	{
		$result = RemoveSymbols($result);
	}
	
	return $result;
}

?>