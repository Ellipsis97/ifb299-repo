<?PHP
//Flag that this is a parent file.
//Enabling included files to run.
define('CanRun', 1);

//Load external scripts.
require './phpclasses/db.php';
require './phpclasses/session.php';
require './phpclasses/dbhelper.php';
require './phpclasses/navbar.php';

//Create database and session manager objects.
$db = new DatabaseConnector();
$session = new Session("UserSession");

//Check to see if this session has expired.
if (!$session->IsAuthed())
{
	//Session expired, redirect to the login page.
	$session->HardDelete();
	header("Location: /");
	exit;
}

//Get user details and ensure the session has not been forged...
if (!CheckLogin($db, $session))
{
	//Session invalid, redirect to the login page.
	$session->HardDelete();
	header("Location: /");
	exit;
}

//Get user personal details so we can customise page contents.
$userID = $session->Get("user-id");
$userDetails = GetUserDetails($db, $userID);

//Redirect if we don't have clearance.
if ($userDetails['clearance'] < 3)
{
	header("Location: /403.php");
	exit;
}

$result = $db->Fetch("ifb299.smokingfines", "", "");
if ($result !== false)
{	
	//Table values
	$tableStart = "<table class=\"table\"><tbody>";
	$tableHeading = AddHeadingsToTable();
	$tableEnd = "</tbody></table>";
	$tableOutput = "";
	
	$rowCount = 0;
	
	while ($row = $result->fetch_assoc())
	{
		AddRowToTable($tableOutput, $row);
		$rowCount++;
	}
	
	$result->free();
}

//We're done with the database connection and result objects
//so now were delete them.
unset($db);

function AddHeadingsToTable()
{
	$result = "<tr><th>Fine ID</th>";
	$result .= "<th>First Name</th>";
	$result .= "<th>Last Name</th>";
	$result .= "<th>Department</th>";
	$result .= "<th>Supervisor</th>";
	$result .= "<th>Date</th>";
	$result .= "<th>Time</th>";
	$result .= "<th>Location</th></tr>";
	
	return $result;
}

function AddRowToTable(&$outputString, $row)
{	
	$fineID = $row['id'];
	$firstName = $row['firstName'];
	$lastName = $row['lastName'];
	$department = $row['department'];
	$supervisor = $row['supervisor'];
	$date = $row['date'];
	$time = $row['time'];
	$location = $row['location'];

	$outputString .= "<tr><td>".$fineID."</td>";
	$outputString .= "<td>".$firstName."</td>";
	$outputString .= "<td>".$lastName."</td>";
	$outputString .= "<td>".$department."</td>";
	$outputString .= "<td>".$supervisor."</td>";
	$outputString .= "<td>".$date."</td>";
	$outputString .= "<td>".$time."</td>";
	$outputString .= "<td>".$location."</td></tr>";
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="" />
	<meta name="author" content="" />
	<link rel="icon" type="image/png" href="images/favicon.ico" />
	<title>PH&S: View Smoking Fines</title>
	<!-- Bootstrap core scripts -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<!-- Page specific scripts -->
	<script src="js/navbar.js"></script>
	<script src="js/viewsmokingfines.js"></script>
	<!-- Bootstrap core CSS -->
	<link href="/css/bootstrap.min.css" rel="stylesheet" />
	<!-- Page specific CSS -->
	<link href="/css/sidebar.css" rel="stylesheet" />
	<link href="/css/viewpages.css" rel="stylesheet" />
</head>
<body>
	<div id="wrapper">
		<?PHP PrintNavBar($userDetails); ?>
		<div id="page-content-wrapper" class="container-fluid">
			<div class="container">
				<!-- Navigation Toggle Button -->
				<a href="#menu-toggle" class="btn btn-default" id="menu-toggle"><span class="glyphicon glyphicon-menu-hamburger"></span><span> Hide Navigation</span></a>
				<!-- Jumbotron -->
				<div class="jumbotron">
					<h1>Smoking Fine Search</h1>
					<p>Please enter search options below...</p>
					<!-- Smoking Fine Search Form -->
					<form id="search-form" class="form-horizontal">
						<div class="form-group">
							<!-- Search Input and Button -->
							<div class="col-sm-10">
								<div class="input-group">
									<label class="sr-only" for="search-value">Search Value</label>
									<input type="text" class="form-control" aria-label="Text input" id="search-value" name="search-value" placeholder="Enter search value and press enter to submit...">
									<div class="input-group-btn" role="group" aria-label="...">
										<button type="submit" class="btn btn-default" id="submit-button">Search</button>
									</div>
								</div>
							</div>
							<!-- Search By Type -->
							<div class="col-sm-2">
								<label class="sr-only" for="search-type">Search Type</label>
								<select class="col-sm-2 form-control" id="search-type" name="search-type">
									<option value="-1">Search By...</option>
									<option value="1">ID</option>
									<option value="2">First Name</option>
									<option value="3">Last Name</option>
									<option value="4">Department</option>
									<option value="5">Supervisor</option>
									<option value="6">Date</option>
									<option value="7">Time</option>
									<option value="8">Location</option>
								</select>
							</div>
						</div>
						<!-- Table Error -->
						<div class="form-group" id="search-table-error">
							<div class="col-sm-12">
								<div class="alert alert-warning" role="alert"><span><b>Error: </b></span><span></span></div>
							</div>
						</div>
						<!-- Result Table -->
						<div class="form-group" id="search-table-section">
							<div class="col-sm-12">
								<table class="table" id="search-table"></table>
							</div>
						</div>
					</form>
				</div>
				<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
					<!-- Smoking Fines -->
					<div class="panel panel-default panel-info">
						<div class="panel-heading" role="tab" id="headingOne">
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse" data-parent="#accordion" 
								href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
									Smoking Fines
								</a>
							</h4>
						</div>
						<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
							<div class="panel-body" id="unpaid-table-section">
								<?PHP print($tableStart.$tableHeading.$tableOutput.$tableEnd); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Logout Confirmation Modal -->
	<?PHP PrintLogOutModal(); ?>
</body>
</html>