<?PHP
//Flag that this is a parent file.
//Enabling included files to run.
define("CanRun", 1);

//Load external scripts.
require './phpclasses/db.php';
require './phpclasses/session.php';
require './phpclasses/dbhelper.php';
require './phpclasses/utility.php';
require './phpclasses/navbar.php';
require './phpclasses/constants.php';

//Create database and session manager objects.
$db = new DatabaseConnector();
$session = new Session("UserSession");

//Check to see if this session has expired.
if (!$session->IsAuthed())
{
	$session->HardDelete();
	header("Location: /");
	exit;
}

//Get user details and ensure the session has not been forged...
if (!CheckLogin($db, $session))
{
	//Session invalid, redirect to the login page.
	$session->HardDelete();
	header("Location: /");
	exit;
}

//Get user personal details so we can customise page contents.
$userID = $session->Get("user-id");
$userDetails = GetUserDetails($db, $userID);

//Redirect if we don't have clearance.
if ($userDetails['clearance'] == 1 || $userDetails['clearance'] == 3)
{
	header("Location: /403.php");
	exit;
}

//Declare error variables so we can
//give feedback to the user.
$showError = false;
$errorText = "";

$submitted = !empty($_POST);

if ($submitted)
{
	$showError = !IssueSmokingFine($db, $errorText);
}

function IssueSmokingFine(&$db, &$errorText)
{
	//Get the values submitted by the form.
	$firstName = DBSafeText($_POST['first-name']);
	$lastName = DBSafeText($_POST['last-name']);
	$department = DBSafeText($_POST['department']);
	$supervisor = DBSafeText($_POST['supervisor']);
	$date = trim($_POST['date']);
	$time = trim($_POST['time']);
	$location = DBSafeText($_POST['location']);
	
	if (ValidateFineValues($errorText, $firstName, $lastName, $department, $supervisor, $date, $time, $location) == false)
	{
		return false;
	}
	
	//Generate an array of field names and values.
	$fields = ["firstName", "lastName", "department", "supervisor", "date", "time", "location"];
	$values = [$firstName, $lastName, $department, $supervisor, $date, $time, $location];
	
	//Generate a INSERT query and run it.
	$result = $db->Insert("smokingfines", $fields, $values);
	
	return true;
}

function ValidateFineValues(&$errorText, $firstName, $lastName, $department, $supervisor, $date, $time, $location)
{	
	//First name.
	if (empty($firstName) || strlen($firstName) > MAX_LENGTH_NAME ||
		preg_match('/[0-9]+/', $firstName) === 1)
	{
		$errorText = "First name value contains numbers or is an invalid length.";
		return false;
	}
	
	//Last name.
	if (empty($lastName) || strlen($lastName) > MAX_LENGTH_NAME ||
		preg_match('/[0-9]+/', $lastName) === 1)
	{		
		$errorText = "Last name value contains numbers or is an invalid length.";
		return false;
	}
	
	//Department.
	if (empty($department) || strlen($department) > MAX_LENGTH_DEPARTMENT ||
		preg_match('/[0-9]+/', $department) === 1)
	{		
		$errorText = "Department value contains numbers or is an invalid length.";
		return false;
	}
	
	//Supervisor name.
	if (empty($supervisor) || strlen($supervisor) > MAX_LENGTH_NAME ||
		preg_match('/[0-9]+/', $supervisor) === 1)
	{
		$errorText = "Supervisor name value contains numbers or is an invalid length.";
		return false;
	}
	
	//Date issued.
	if (preg_match('/[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}/', $date) === 0)
	{
		$errorText = "Date format is invalid.";
		return false;
	}
	
	//Time issued.
	if (preg_match('/[0-9]{1,2}:[0-9]{1,2}/', $time) === 0)
	{
		$errorText = "Time format is invalid.";
		return false;
	}
	
	$time = explode(":", $time);
	if ($time[0] > 24 || $time[1] > 60)
	{
		$errorText = "Time input's hour or minute counts are invalid.";
		return false;
	}	
	
	//Location.
	if (empty($location) || strlen($location) > MAX_LENGTH_LOCATION)
	{		
		$errorText = "Location value is an invalid length.";
		return false;
	}
	
	return true;
}

//We're done with the database connection object
//so now we delete it.
unset($db);
	
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <link rel="icon" type="image/png" href="images/favicon.ico"/>
    <title>PH&S: Issue Smoking Fine</title>
    <!-- Bootstrap core scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
	<!-- Page specific scripts -->
	<script src="js/navbar.js"></script>
    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet"/>
	<!-- Page specific CSS -->
    <link href="/css/sidebar.css" rel="stylesheet"/>
</head>
<body>
	<div id="wrapper">
		<?PHP PrintNavBar($userDetails); ?>
		<div id="page-content-wrapper" class="container-fluid">
			<div class="container">
				<!-- Navigation Toggle Button -->		
				<a href="#menu-toggle" class="btn btn-default" id="menu-toggle"><span class="glyphicon glyphicon-menu-hamburger"></span><span> Hide Navigation</span></a>
				<!-- Jumbotron -->
				<div>
					<div class="jumbotron">
						<h1>Issue Smoking Fine</h1>
						<p>Please enter the details for the smoking fine below...</p>
					</div>
<?PHP				
if ($submitted)
{
	if ($showError == true)
	{
		//Report an error.
		print("<div class=\"alert alert-danger\" role=\"alert\"><b>Error:</b> ".$errorText."</div>");
	}
	else
	{
		//Report success.
		print("<div class=\"alert alert-success\" role=\"alert\"><b>Success:</b> The smoking fine has been submitted!</div>");
	}
}
?>
					<!-- Citation form -->
					<form class="form-horizontal" action="issuesmokingfine.php" method="post">
						<!-- VIOLATOR FIRST NAME -->
						<div class="form-group">
							<label for="first-name" class="col-md-2 control-label">First Name</label>
							<div class="col-md-4">
								<input type="text" class="form-control" id="first-name" name="first-name" placeholder="First Name" maxlength="<?PHP print(MAX_LENGTH_NAME); ?>" required="true" autofocus="true">
							</div>
						</div>
						<!-- LAST NAME -->
						<div class="form-group">
							<label for="last-name" class="col-md-2 control-label">Last Name</label>
							<div class="col-md-4">
								<input type="text" class="form-control" id="last-name" name="last-name" placeholder="Last Name" maxlength="<?PHP print(MAX_LENGTH_NAME); ?>" required="true">
							</div>
						</div>
						<!-- DEPARTMENT -->
						<div class="form-group">
							<label for="department" class="col-md-2 control-label">Department</label>
							<div class="col-md-4">
								<input type="text" class="form-control" id="department" name="department" placeholder="Department" maxlength="<?PHP print(MAX_LENGTH_DEPARTMENT); ?>" required="true">
							</div>
						</div>
						<!-- SUPERVISOR -->
						<div class="form-group">
							<label for="supervisor" class="col-md-2 control-label">Supervisor</label>
							<div class="col-md-4">
								<input type="text" class="form-control" id="supervisor" name="supervisor" placeholder="Supervisor" maxlength="<?PHP print(MAX_LENGTH_NAME); ?>" required="true">
							</div>
						</div>
						<!-- DATE -->
						<div class="form-group">
							<label for="date" class="col-md-2 control-label">Date</label>
							<div class="col-md-4">
								<input type="date" class="form-control" id="date" name="date" required="true">
							</div>
						</div>
						<!-- Time -->
						<div class="form-group">
							<label for="time" class="col-md-2 control-label">Time</label>
							<div class="col-md-4">
								<input type="text" class="form-control" id="time" name="time" placeholder="e.g. 14:32" maxlength="<?PHP print(MAX_LENGTH_TIME_STRING); ?>" required="true">
							</div>
						</div>
						<!-- Location -->
						<div class="form-group">
							<label for="location" class="col-md-2 control-label">Location on Campus</label>
							<div class="col-md-4">
								<input type="text" class="form-control" id="location" name="location" placeholder="Location" maxlength="<?PHP print(MAX_LENGTH_LOCATION); ?>" required="true">
							</div>
						</div>
						<!-- Buttons -->
						<div class="form-group">
							<div class="btn-group col-md-offset-2 col-md-3" role="group" aria-label="...">
								<button type="submit" class="btn btn-lg btn-default" id="submit-button">Submit Fine</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- Logout Confirmation Modal -->
	<?PHP PrintLogOutModal(); ?>
</body>
</html>