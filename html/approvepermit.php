<?PHP
if (empty($_GET))
{
	die();
}

//Flag that this is a parent file.
//Enabling included files to run.
define('CanRun', 1);

//Load external scripts.
require './phpclasses/db.php';
require './phpclasses/session.php';
require './phpclasses/dbhelper.php';
require './phpclasses/utility.php';

//Create database and session manager objects.
$db = new DatabaseConnector();
$session = new Session("UserSession");

//Check to see if this session has expired.
if (!$session->IsAuthed())
{
	//Session expired, redirect to the login page.
	$session->HardDelete();
	header("Location: /");
	exit;
}

//Get user details and ensure the session has not been forged...
if (!CheckLogin($db, $session))
{
	//Session invalid, redirect to the login page.
	$session->HardDelete();
	header("Location: /");
	exit;
}

$id = RemoveSymbols($_GET['id']);

//UPDATE `ifb299`.`permits` SET `permitStatus`='Current' WHERE `id`='10';
$db->Update("permits", Array("permitStatus"), Array("Current"), "WHERE `id`='$id'");

$result = $db->Fetch("ifb299.permits", "", "WHERE permitStatus=\"Pending\" OR permitStatus=\"Current\"");
if ($result !== false)
{		
	$output = "[";

	while ($row = $result->fetch_assoc())
	{
		//Add comma delimiters, unless this is the first iteration.
		if ($output != "[")
		{
			$output .= ",";
		}
		
		$output .= '{"id":"'.$row["id"].'",';
		$output .= '"firstName":"'.$row["firstName"].'",';
		$output .= '"lastName":"'.$row["lastName"].'",';
		$output .= '"dob":"'.$row["dob"].'",';
		$output .= '"department":"'.$row["department"].'",';
		$output .= '"vehicleType":"'.$row["vehicleType"].'",';
		$output .= '"vehicleReg":"'.$row["vehicleReg"].'",';
		$output .= '"startDate":"'.$row["startDate"].'",';
		$output .= '"endDate":"'.$row["endDate"].'",';
		$output .= '"permitStatus":"'.$row["permitStatus"].'",';
		$output .= '"durationType":"'.$row["durationType"].'"}';
	}
	
	$output .= "]";
	print($output);
	
	$result->free();
}
else
{
	print('[{"errorText":"Something went wrong."}]');
}

//We're done with the database connection and result objects
//so now were delete them.
unset($db);

?>