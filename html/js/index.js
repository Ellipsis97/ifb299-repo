$(document).ready(function()
{
	var guestAppVisible = false;
	
	//Bind function to the onclick event of the jumbotron button.
	$("#guest-app-button").on("click", ToggleApplication);	
	
	//When the login modal is revealed, focus the email input.
	$('#login-modal').on('shown.bs.modal', function()
	{
		$('#email').focus()
	});
	
	function ToggleApplication()
	{		
		if (!guestAppVisible)
		{		
			$("#guest-app-section").show(300);
			$("#first-name").focus();
		}
		else
		{
			$("#guest-app-section").hide(300);
		}
		
		guestAppVisible = !guestAppVisible;
	}
	
	//To start, hide the time input.
	$('#time-form').hide();

	//When the permit duration type changes...
	$('#permit-duration').change(function()
	{
		var value = $(this).find("option:selected").text();
		
		//This doesn't remove the attribute when using jquery
		//and could be fixed by using regular javascript to set it but functionally it works.
		if (value == 'Hourly Pass')
		{
			//Show time input.
			$('#time-form').show(300);
			$('#start-time').attr('required', true);
		}
		else
		{
			//Hide time input.
			$('#time-form').hide(300);
			$('#start-time').attr('required', false);
		}
	});
});