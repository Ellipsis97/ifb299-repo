var XHRBusy = false;

$(document).ready(function()
{
	//Bind an event for when a user resolves a citation.
	$("#resolve-submit").on("click", ResolveCitation);
	
	//Bind a submit event listener and function to the search form element.
	$("#search-form").bind("submit", function(event)
	{
		//Prevent the search form actually submitting.
		event.preventDefault();
		
		//Prevent searches if the type is invalid.
		if ($("#search-type")[0].value == "-1")
		{
			return;
		}
		
		CitationSearch();
	});
	
	function ResolveCitationCallback(response)
	{
		XHRBusy = false;
	
		//Parse the resulting JSON document.
		var arr = JSON.parse(response);

		//Generate a table from the JSON.
		var tableStart = "<table class=\"table\">";		
		
		unresolvedTableStart = tableStart + "<tr><th>ID</th><th>First Name</th><th>Last Name</th>\
<th>Department</th><th>Date</th>\
<th>Time</th><th>Description</th><th>Resolve</th></tr>";

		tableStart += "<tr><th>ID</th><th>First Name</th><th>Last Name</th>\
<th>Department</th><th>Date</th>\
<th>Time</th><th>Description</th><th>Resolution Date</th><th>Resolution Description</th></tr>";

		var citations = "";
		var unresolvedCitations = "";
		for (var i = 0; i < arr.length; i++)
		{
			if (arr[i].isResolved == 1)
			{
				//Create a row for the citations table.
				citations += "<tr><td>" +
				arr[i].id +
				"</td><td>" +
				arr[i].firstName +
				"</td><td>" +
				arr[i].lastName +
				"</td><td>" +
				arr[i].department +
				"</td><td>" +
				arr[i].date +
				"</td><td>" +
				arr[i].time +
				"</td><td>" +
				arr[i].description +
				"</td><td>" +
				arr[i].resolutionDate +
				"</td><td>" +
				arr[i].resolutionDescription +
				"</td></tr>";
			}
			else if (arr[i].isResolved == 0)
			{			
				//Create a row for the unresolved citations table.
				unresolvedCitations += "<tr><td>" +
				arr[i].id +
				"</td><td>" +
				arr[i].firstName +
				"</td><td>" +
				arr[i].lastName +
				"</td><td>" +
				arr[i].department +
				"</td><td>" +
				arr[i].date +
				"</td><td>" +
				arr[i].time +
				"</td><td>" +
				arr[i].description +
				"</td><td><button class=\"btn btn-default\" onclick=\"OpenResolveModal(this, "+ arr[i].id + ")\"><span class=\"glyphicon glyphicon-edit\"></span></button></td></tr>";
			}
		}
		tableEnd = "</table>";
		
		//Output results to tables.
		document.getElementById("unresolved-table").innerHTML = unresolvedTableStart + unresolvedCitations + tableEnd;
		document.getElementById("citations-table").innerHTML = tableStart + citations + tableEnd;
		
		//Show the table now we're done
		$("#unresolved-table").show();
		$("#citations-table").show();
	}
	
	function ResolveCitation(event)
	{	
		//Don't let us do any async requests
		//until the last one is done.
		if (XHRBusy)
		{
			return;
		}
		XHRBusy = true;
		
		$('#resolveModal').modal('hide');
		
		//Hide the tables.
		$("#unresolved-table").hide();
		$("#citations-table").hide();
		
		//Creates a XHR object that visits a php page.
		var xmlhttp = new XMLHttpRequest();
		var url = "./resolveCitation.php";
		var post = "id=" + $('#resolve-id')[0].value + "&desc=" + $('#resolution-description')[0].value;
		
		xmlhttp.onreadystatechange = function()
		{
			if (this.readyState == 4 && this.status == 200)
			{
				ResolveCitationCallback(this.responseText);
			}
		}
		xmlhttp.open("POST", url, true);
		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xmlhttp.send(post);
	}
	
	//Generates the GET part of the request URL.
	function MakeSearchGet()
	{
		var searchType = $("#search-type")[0].value;
		var searchValue = $("#search-value")[0].value;
		
		return "?st=" + searchType + "&sv=" + searchValue + "&stable=citations";
	}
	
	function CitationSearch()
	{
		//Don't let us do any async requests
		//until the last one is done.
		if (XHRBusy)
		{
			return;
		}
		XHRBusy = true;
		
		//Hide the table and error section.
		$("#search-table-section").hide();
		$("#search-table-error").hide();
		
		//Creates a XHR object that visits a php page.
		var xmlhttp = new XMLHttpRequest();
		var url = "./search.php" + MakeSearchGet();

		xmlhttp.onreadystatechange = function()
		{
			if (this.readyState == 4 && this.status == 200)
			{
				CitationSearchCallback(this.responseText);
			}
		}
		xmlhttp.open("GET", url, true);
		xmlhttp.send();
	}
	
	//This function is called when the XHR finishes.
	function CitationSearchCallback(response)
	{
		XHRBusy = false;
		
		//Parse the resulting JSON document.
		var arr = JSON.parse(response);
		
		//If there are no results, show an error.
		if (arr.length == 0)
		{
			$("#search-table-error").show();
			$("#search-table-error").delay(10000).fadeOut(300);
			$("#search-table-error").find("span:last").text("No results found.");
			
			return;
		}
		
		if (arr[0].errorText !== undefined)
		{
			$("#search-table-error").show();
			$("#search-table-error").delay(10000).fadeOut(300);
			$("#search-table-error").find("span:last").text(arr[0].errorText);
			
			return;
		}
		
		//Generate a table from the JSON.
		var out = "<table class=\"table\">";		
		out += "<tr><th>ID</th><th>First Name</th><th>Last Name</th>\
<th>Department</th><th>Date</th>\
<th>Time</th><th>Description</th><th>Resolution Date</th><th>Resolution Description</th></tr>";
		for (var i = 0; i < arr.length; i++)
		{
			out += "<tr><td>" +
			arr[i].id +
			"</td><td>" +
			arr[i].firstName +
			"</td><td>" +
			arr[i].lastName +
			"</td><td>" +
			arr[i].department +
			"</td><td>" +
			arr[i].date +
			"</td><td>" +
			arr[i].time +
			"</td><td>" +
			arr[i].description +
			"</td><td>" +
			arr[i].resolutionDate +
			"</td><td>" +
			arr[i].resolutionDescription +
			"</td></tr>";
		}
		out += "</table>";
		
		//Output results to table.
		document.getElementById("search-table").innerHTML = out;
		
		//Show the table.
		$("#search-table-section").show();
	}
});

function OpenResolveModal(button, id)
{
	if (XHRBusy == true)
	{
		return;
	}
	
	//Remove all rows from the table except the heading row.
	$('#modal-table tr:not(:first)').remove();
	
	//Find the closest row, then all child cells.
	var row = $(button).closest("tr");
    tds = row.find("td");
	
	//Transfer row of data to modal window.
	var tableRow = "<tr>";
	for (var i = 0; i < tds.length - 1; i++)
	{
		tableRow += "<td>" + $(tds[i]).text() + "</td>"
	}
	tableRow += "</tr>";
	
	$('#modal-table tbody').append(tableRow);
	
	//Store row id, show the modal.
	$('#resolve-id')[0].value = id;
	$('#resolveModal').modal('show');
}