$(document).ready(function()
{
	//Attach change event to checkbox.
	$("#permit-number-known").on("change", PermitNumberKnownCheck);
	
	//Disable inputs based on checkbox state.
	function PermitNumberKnownCheck(event)
	{
		if ($("#permit-number-known")[0].checked)
		{
			//Toggle on.
			$("#permit-number")[0].disabled = false;
			$("#vehicle-type")[0].disabled = true;
			$("#vehicle-reg")[0].disabled = true;
			
			$('#permit-number').attr('required', true);
			$('#vehicle-type').attr('required', false);
			$('#vehicle-reg').attr('required', false);
		}
		else
		{
			//Toggle off.
			$("#permit-number")[0].disabled = true;
			$("#vehicle-type")[0].disabled = false;
			$("#vehicle-reg")[0].disabled = false;
			
			$('#permit-number').attr('required', false);
			$('#vehicle-type').attr('required', true);
			$('#vehicle-reg').attr('required', true);
		}
	}	
});