$(document).ready(function()
{
	var navVisible = true;
	
	//Bind an mouse click event to the navigation button.
	$("#menu-toggle").click(function(e)
	{
		//Prevent this element from doing default link behaviour.
		e.preventDefault();
		
		//Toggle the navigation menu.
		$("#wrapper").toggleClass("toggled");
		
		//Update the button text.
		if (navVisible)
		{
			$("#menu-toggle").find("span:last").text(" Show Navigation");
		}
		else
		{
			$("#menu-toggle").find("span:last").text(" Hide Navigation");	
		}
		
		navVisible = !navVisible;
	});
});