var XHRBusy = false;

$(document).ready(function()
{	
	//Bind a submit event listener and function to the search form element.
	$("#search-form").bind("submit", function(event)
	{
		//Prevent the search form actually submitting.
		event.preventDefault();
		
		//Prevent searches if the type is invalid.
		if ($("#search-type")[0].value == "-1")
		{
			return;
		}
		
		PermitSearch();
	});
	
	//Generates the GET part of the request URL.
	function MakeSearchGet()
	{
		var searchType = $("#search-type")[0].value;
		var searchValue = $("#search-value")[0].value;
		
		return "?st=" + searchType + "&sv=" + searchValue + "&stable=permits";
	}
	
	function PermitSearch()
	{
		//Don't let us do any async requests
		//until the last one is done.
		if (XHRBusy)
		{
			return;
		}
		XHRBusy = true;
		
		//Hide the table and error section.
		$("#search-table-section").hide();
		$("#search-table-error").hide();
		
		//Creates a XHR object that visits a php page.
		var xmlhttp = new XMLHttpRequest();
		var url = "./search.php" + MakeSearchGet();

		xmlhttp.onreadystatechange = function()
		{
			if (this.readyState == 4 && this.status == 200)
			{
				PermitSearchCallback(this.responseText);
			}
		}
		xmlhttp.open("GET", url, true);
		xmlhttp.send();
	}
	
	//This function is called when the XHR finishes.
	function PermitSearchCallback(response)
	{
		XHRBusy = false;
		
		//Parse the resulting JSON document.
		var arr = JSON.parse(response);
		
		//If there are no results, show an error.
		if (arr.length == 0)
		{
			$("#search-table-error").show();
			$("#search-table-error").delay(10000).fadeOut(300);
			$("#search-table-error").find("span:last").text("No results found.");
			
			return;
		}
		
		if (arr[0].errorText !== undefined)
		{
			$("#search-table-error").show();
			$("#search-table-error").delay(10000).fadeOut(300);
			$("#search-table-error").find("span:last").text(arr[0].errorText);
			
			return;
		}
		
		//Generate a table from the JSON.
		var out = "<table class=\"table\">";		
		out += "<tr><th>ID</th><th>First Name</th><th>Last Name</th>\
<th>Date of Birth</th><th>Department</th><th>Vehicle Type</th>\
<th>Vehicle Registration</th><th>Start Date</th><th>End Date</th><th>Duration Type</th></tr>";
		for (var i = 0; i < arr.length; i++)
		{
			out += "<tr><td>" +
			arr[i].id +
			"</td><td>" +
			arr[i].firstName +
			"</td><td>" +
			arr[i].lastName +
			"</td><td>" +
			arr[i].dob +
			"</td><td>" +
			arr[i].department +
			"</td><td>" +
			arr[i].vehicleType +
			"</td><td>" +
			arr[i].vehicleReg +
			"</td><td>" +
			arr[i].startDate +
			"</td><td>" +
			arr[i].endDate +
			"</td><td>" +
			arr[i].durationType +
			"</td></tr>";
		}
		out += "</table>";
		
		//Output results to table.
		document.getElementById("search-table").innerHTML = out;
		
		//Show the table.
		$("#search-table-section").show();
	}
});

function PermitApproveCallback(response)
{
	XHRBusy = false;
	
	//Parse the resulting JSON document.
	var arr = JSON.parse(response);
	
	//Generate a table from the JSON.
	var tableStart = "<table class=\"table\">";		
	tableStart += "<tr><th>ID</th><th>First Name</th><th>Last Name</th>\
<th>Date of Birth</th><th>Department</th><th>Vehicle Type</th>\
<th>Vehicle Registration</th><th>Start Date</th><th>End Date</th><th>Duration Type</th>";
	var tableStartPending = tableStart+"<th>Approve</th></tr>";
	tableStart += "</tr>";

	var pendingRows = "";
	var currentRows = "";
	for (var i = 0; i < arr.length; i++)
	{
		if (arr[i].permitStatus == "Current")
		{
			//Create a row for the Current Permits table.
			currentRows += "<tr><td>" +
			arr[i].id +
			"</td><td>" +
			arr[i].firstName +
			"</td><td>" +
			arr[i].lastName +
			"</td><td>" +
			arr[i].dob +
			"</td><td>" +
			arr[i].department +
			"</td><td>" +
			arr[i].vehicleType +
			"</td><td>" +
			arr[i].vehicleReg +
			"</td><td>" +
			arr[i].startDate +
			"</td><td>" +
			arr[i].endDate +
			"</td><td>" +
			arr[i].durationType +
			"</td></tr>";
		}
		else if (arr[i].permitStatus == "Pending")
		{			
			//Create a row for the Pending Permits table.
			pendingRows += "<tr><td>" +
			arr[i].id +
			"</td><td>" +
			arr[i].firstName +
			"</td><td>" +
			arr[i].lastName +
			"</td><td>" +
			arr[i].dob +
			"</td><td>" +
			arr[i].department +
			"</td><td>" +
			arr[i].vehicleType +
			"</td><td>" +
			arr[i].vehicleReg +
			"</td><td>" +
			arr[i].startDate +
			"</td><td>" +
			arr[i].endDate +
			"</td><td>" +
			arr[i].durationType +
			"</td><td><button class=\"btn btn-default\" onclick=\"Approve("+arr[i].id+")\"><span class=\"glyphicon glyphicon-ok\"></span></button>" +
			"<button class=\"btn btn-default\" onclick=\"Remove("+arr[i].id+")\"><span class=\"glyphicon glyphicon-remove\"></span></button>" +
			"</td></tr>";
		}
	}
	tableEnd = "</table>";
	
	//Output results to tables.
	document.getElementById("pending-table-section").innerHTML = tableStartPending + pendingRows + tableEnd;
	document.getElementById("current-table-section").innerHTML = tableStart + currentRows + tableEnd;
	
	//Show the table now we're done
	$("#pending-table-section").show();
	$("#current-table-section").show();
}

function Approve(id)
{
	//Don't let us do any async requests
	//until the last one is done.
	if (XHRBusy)
	{
		return;
	}
	XHRBusy = true;
	
	//Hide the tables until we're done
	$("#pending-table-section").hide();
	$("#current-table-section").hide();
	
	//Creates a XHR object that visits a php page.
	var xmlhttp = new XMLHttpRequest();
	var url = "./approvepermit.php?id=" + id;

	xmlhttp.onreadystatechange = function()
	{
		if (this.readyState == 4 && this.status == 200)
		{
			PermitApproveCallback(this.responseText);
		}
	}
	xmlhttp.open("GET", url, true);
	xmlhttp.send();
}

function PermitRemoveCallback(response)
{
	XHRBusy = false;
	
	//Parse the resulting JSON document.
	var arr = JSON.parse(response);
	
	//Generate a table from the JSON.
	var out = "<table class=\"table\">";		
	out += "<tr><th>ID</th><th>First Name</th><th>Last Name</th>\
<th>Date of Birth</th><th>Department</th><th>Vehicle Type</th>\
<th>Vehicle Registration</th><th>Start Date</th><th>End Date</th><th>Duration Type</th></tr>";
	for (var i = 0; i < arr.length; i++)
	{
		out += "<tr><td>" +
		arr[i].id +
		"</td><td>" +
		arr[i].firstName +
		"</td><td>" +
		arr[i].lastName +
		"</td><td>" +
		arr[i].dob +
		"</td><td>" +
		arr[i].department +
		"</td><td>" +
		arr[i].vehicleType +
		"</td><td>" +
		arr[i].vehicleReg +
		"</td><td>" +
		arr[i].startDate +
		"</td><td>" +
		arr[i].endDate +
		"</td><td>" +
		arr[i].durationType +
		"</td><td><button class=\"btn btn-default\" onclick=\"Approve("+arr[i].id+")\"><span class=\"glyphicon glyphicon-ok\"></span></button>" +
		"<button class=\"btn btn-default\" onclick=\"Remove("+arr[i].id+")\"><span class=\"glyphicon glyphicon-remove\"></span></button>" +
		"</td></tr>";
	}
	out += "</table>";
	
	//Output results to table.
	document.getElementById("pending-table-section").innerHTML = out;
	
	//Show the table now we're done
	$("#pending-table-section").show();
}

function Remove(id)
{
	//Don't let us do any async requests
	//until the last one is done.
	if (XHRBusy)
	{
		return;
	}
	XHRBusy = true;
	
	//Hide the table until we're done
	$("#pending-table-section").hide();
	
	//Creates a XHR object that visits a php page.
	var xmlhttp = new XMLHttpRequest();
	var url = "./removepermit.php?id=" + id;

	xmlhttp.onreadystatechange = function()
	{
		if (this.readyState == 4 && this.status == 200)
		{
			PermitRemoveCallback(this.responseText);
		}
	}
	xmlhttp.open("GET", url, true);
	xmlhttp.send();
}

function PermitRenewCallback(response)
{
	XHRBusy = false;
	
	//Parse the resulting JSON document.
	var arr = JSON.parse(response);
	
	//Generate a table from the JSON.
	var out = "<table class=\"table\">";		
	out += "<tr><th>ID</th><th>First Name</th><th>Last Name</th>\
<th>Date of Birth</th><th>Department</th><th>Vehicle Type</th>\
<th>Vehicle Registration</th><th>Start Date</th><th>End Date</th><th>Duration Type</th></tr>";
	for (var i = 0; i < arr.length; i++)
	{
		out += "<tr><td>" +
		arr[i].id +
		"</td><td>" +
		arr[i].firstName +
		"</td><td>" +
		arr[i].lastName +
		"</td><td>" +
		arr[i].dob +
		"</td><td>" +
		arr[i].department +
		"</td><td>" +
		arr[i].vehicleType +
		"</td><td>" +
		arr[i].vehicleReg +
		"</td><td>" +
		arr[i].startDate +
		"</td><td>" +
		arr[i].endDate +
		"</td><td>" +
		arr[i].durationType +
		"</td><td><button class=\"btn btn-default\" onclick=\"Renew("+arr[i].id+")\"><span class=\"glyphicon glyphicon-refresh\"></span></button>" +
		"</td></tr>";
	}
	out += "</table>";
	
	//Output results to table.
	document.getElementById("expired-table-section").innerHTML = out;
	
	//Show the table now we're done
	$("#expired-table-section").show();
}

function Renew(id)
{
	//Don't let us do any async requests
	//until the last one is done.
	if (XHRBusy)
	{
		return;
	}
	XHRBusy = true;
	
	//Hide the table until we're done
	$("#expired-table-section").hide();
	
	//Creates a XHR object that visits a php page.
	var xmlhttp = new XMLHttpRequest();
	var url = "./renewpermit.php?id=" + id;

	xmlhttp.onreadystatechange = function()
	{
		if (this.readyState == 4 && this.status == 200)
		{
			PermitRenewCallback(this.responseText);
		}
	}
	xmlhttp.open("GET", url, true);
	xmlhttp.send();
}