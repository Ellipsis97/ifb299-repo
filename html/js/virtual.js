var XHRBusy = false;

function PermitRenewCallback(response)
{
	XHRBusy = false;
	
	//Parse the resulting JSON document.
	var arr = JSON.parse(response);
	
	//Generate a table from the JSON.
	var out = "<table class=\"table\"><tr><th>ID</th><th>First Name</th><th>Last Name</th>\
<th>Date of Birth</th><th>Department</th><th>Vehicle Type</th>\
<th>Vehicle Registration</th><th>Start Date</th><th>End Date</th></tr>";

	for (var i = 0; i < arr.length; i++)
	{
		out += "<tr><td>" +
		arr[i].id +
		"</td><td>" +
		arr[i].firstName +
		"</td><td>" +
		arr[i].lastName +
		"</td><td>" +
		arr[i].dob +
		"</td><td>" +
		arr[i].department +
		"</td><td>" +
		arr[i].vehicleType +
		"</td><td>" +
		arr[i].vehicleReg +
		"</td><td>" +
		arr[i].startDate +
		"</td><td>" +
		arr[i].endDate +
		"</td></tr>";
	}
	out += "</table>";
	
	//Output results to table.
	document.getElementById("user-permit-table").innerHTML = out;
	
	//Show the table now we're done
	$("#user-permit-table").show();
}

function Renew(id, durationType)
{
	//Don't let us do any async requests
	//until the last one is done.
	if (XHRBusy)
	{
		return;
	}
	XHRBusy = true;
	
	//Hide error holder and permit table.
	$("#user-permit-table").hide();
	$("#virtual-error").hide();
	
	//Creates a XHR object that visits a php page.
	var xmlhttp = new XMLHttpRequest();
	var url = "./renewpermit.php?id=" + id + "&durationType=" + durationType;

	//Send async request.
	xmlhttp.onreadystatechange = function()
	{
		if (this.readyState == 4 && this.status == 200)
		{
			PermitRenewCallback(this.responseText);
		}
	}
	xmlhttp.open("GET", url, true);
	xmlhttp.send();
}