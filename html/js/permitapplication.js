$(document).ready(function()
{	
	$('#permit-duration').change(function()
	{
		var value = $(this).find("option:selected").text();
		
		if (value == 'Hourly Pass')
		{
			$('#time-form').removeClass('hide');
		}
		else
		{
			$('#time-form').addClass('hide');
		}
	});
});