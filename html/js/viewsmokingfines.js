var XHRBusy = false;

$(document).ready(function()
{	
	//Bind a submit event listener and function to the search form element.
	$("#search-form").bind("submit", function(event)
	{
		//Prevent the search form actually submitting.
		event.preventDefault();
		
		//Prevent searches if the type is invalid.
		if ($("#search-type")[0].value == "-1")
		{
			return;
		}
		
		FineSearch();
	});
	
	//Generates the GET part of the request URL.
	function MakeSearchGet()
	{
		var searchType = $("#search-type")[0].value;
		var searchValue = $("#search-value")[0].value;
		
		return "?st=" + searchType + "&sv=" + searchValue + "&stable=smokingfines";
	}
	
	function FineSearch()
	{
		//Don't let us do any async requests
		//until the last one is done.
		if (XHRBusy)
		{
			return;
		}
		XHRBusy = true;
		
		//Hide the table and error section.
		$("#search-table-section").hide();
		$("#search-table-error").hide();
		
		//Creates a XHR object that visits a php page.
		var xmlhttp = new XMLHttpRequest();
		var url = "./search.php" + MakeSearchGet();

		xmlhttp.onreadystatechange = function()
		{
			if (this.readyState == 4 && this.status == 200)
			{
				FineSearchCallback(this.responseText);
			}
		}
		xmlhttp.open("GET", url, true);
		xmlhttp.send();
	}
	
	//This function is called when the XHR finishes.
	function FineSearchCallback(response)
	{
		XHRBusy = false;
		
		//Parse the resulting JSON document.
		var arr = JSON.parse(response);
		
		//If there are no results, show an error.
		if (arr.length == 0)
		{
			$("#search-table-error").show();
			$("#search-table-error").delay(10000).fadeOut(300);
			$("#search-table-error").find("span:last").text("No results found.");
			
			return;
		}
		
		if (arr[0].errorText !== undefined)
		{
			$("#search-table-error").show();
			$("#search-table-error").delay(10000).fadeOut(300);
			$("#search-table-error").find("span:last").text(arr[0].errorText);
			
			return;
		}
		
		//Generate a table from the JSON.
		var out = "<table class=\"table\">";		
		out += "<tr><th>ID</th><th>First Name</th><th>Last Name</th>\
<th>Department</th><th>Supervisor</th>\
<th>Date</th><th>Time</th><th>Location</th></tr>";
		for (var i = 0; i < arr.length; i++)
		{
			out += "<tr><td>" +
			arr[i].id +
			"</td><td>" +
			arr[i].firstName +
			"</td><td>" +
			arr[i].lastName +
			"</td><td>" +
			arr[i].department +
			"</td><td>" +
			arr[i].supervisor +
			"</td><td>" +
			arr[i].date +
			"</td><td>" +
			arr[i].time +
			"</td><td>" +
			arr[i].location +
			"</td></tr>";
		}
		out += "</table>";
		
		//Output results to table.
		document.getElementById("search-table").innerHTML = out;
		
		//Show the table.
		$("#search-table-section").show();
	}
});