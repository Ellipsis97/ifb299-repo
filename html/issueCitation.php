<?PHP
//Flag that this is a parent file.
//Enabling included files to run.
define("CanRun", 1);

require './phpclasses/db.php';
require './phpclasses/session.php';
require './phpclasses/dbhelper.php';
require './phpclasses/utility.php';
require './phpclasses/navbar.php';
require './phpclasses/constants.php';

$db = new DatabaseConnector();
$session = new Session("UserSession");

//Check to see if this session has expired.
if (!$session->IsAuthed())
{
	$session->HardDelete();
	header("Location: /");
	exit;
}

//Get user details and ensure the session has not been forged...
if (!CheckLogin($db, $session))
{
	//Session invalid, redirect to the login page.
	$session->HardDelete();
	header("Location: /");
	exit;
}

//Get user personal details so we can customise page contents.
$userID = $session->Get("user-id");
$userDetails = GetUserDetails($db, $userID);

//Declare error variables so we can
//give feedback to the user.
$showError = false;
$errorText = "";

$submitted = !empty($_POST);

if ($submitted)
{
	$showError = !IssueCitation($db, $errorText);
}

function IssueCitation(&$db, &$errorText)
{
	//Get the values submitted by the form.
	$firstName = DBSafeText($_POST['first-name']);
	$lastName = DBSafeText($_POST['last-name']);
	$department = DBSafeText($_POST['department']);
	$date = trim($_POST['date']);
	$time = trim($_POST['time']);
	$description = DBSafeText($_POST['description']);
	
	if (ValidateCitationValues($errorText, $firstName, $lastName, $department, $date, $time, $description) == false)
	{
		return false;
	}
	
	//Generate an INSERT query.
	$fields = ["firstName", "lastName", "department", "date", "time", "description"];
	$values = [$firstName, $lastName, $department, $date, $time, $description];
	
	//Insert this citation into the citations table in the database.
	$result = $db->Insert("citations", $fields, $values);
	
	return true;
}

function ValidateCitationValues(&$errorText, $firstName, $lastName, $department, $date, $time, $description)
{
	//First name.
	if (empty($firstName) || strlen($firstName) > MAX_LENGTH_NAME ||
		preg_match('/[0-9]+/', $firstName) === 1)
	{
		$errorText = "First name value contains numbers or is an invalid length.";
		return false;
	}
	
	//Last name.
	if (empty($lastName) || strlen($lastName) > MAX_LENGTH_NAME ||
		preg_match('/[0-9]+/', $lastName) === 1)
	{
		$errorText = "Last name value contains numbers or is an invalid length.";
		return false;
	}
	
	//Department.
	if (empty($department) || strlen($department) > MAX_LENGTH_DEPARTMENT ||
		preg_match('/[0-9]+/', $department) === 1)
	{
		$errorText = "Department value contains numbers or is an invalid length.";
		return false;
	}
		
	//Date.
	if (preg_match('/[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}/', $date) === 0)
	{
		$errorText = "Date value is in an incorrect format.";
		return false;
	}
		
	//Time.
	if (preg_match('/[0-9]{1,2}:[0-9]{1,2}/', $time) === 0)
	{
		$errorText = "Time value is in an incorrect format.";
		return false;
	}
	
	$time = explode(":", $time);
	if ($time[0] > 24 || $time[1] > 60)
	{
		$errorText = "Time's hours or minutes value is invalid.";
		return false;
	}
	
	//Description.
	if (empty($description) || strlen($description) > MAX_LENGTH_DESCRIPTION)
	{
		$errorText = "Citation description value is an invalid length.";
		return false;
	}
	
	return true;	
}

//We're done with the database connection object
//so now we delete it.
unset($db);
	
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <link rel="icon" type="image/png" href="images/favicon.ico"/>
    <title>PH&S: Issue Citation</title>
    <!-- Bootstrap core scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
	<!-- Page specific scripts -->
	<script src="js/navbar.js"></script>
    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet"/>
	<!-- Page specific CSS -->
    <link href="/css/sidebar.css" rel="stylesheet"/>
</head>
<body>
	<div id="wrapper">
		<?PHP PrintNavBar($userDetails); ?>
		<div id="page-content-wrapper" class="container-fluid">
			<div class="container">
				<!-- Navigation Toggle Button -->		
				<a href="#menu-toggle" class="btn btn-default" id="menu-toggle"><span class="glyphicon glyphicon-menu-hamburger"></span><span> Hide Navigation</span></a>
				<!-- Jumbotron -->
				<div>
					<div class="jumbotron">
						<h1>Issue Citation</h1>
						<p>Please enter the details for the health and safety citation below...</p>
					</div>
<?PHP				
if ($submitted)
{
	if ($showError)
	{
		//Error.
		print("<div class=\"alert alert-danger\" role=\"alert\"><b>Error: </b>".$errorText."</div>");		
	}
	else
	{
		//Success!
		print("<div class=\"alert alert-success\" role=\"alert\"><b>Success:</b> The Citation has been submitted!</div>");
	}
}
?>
					<!-- Citation form -->
					<form class="form-horizontal" action="issuecitation.php" method="post">
						<!-- Violator First Name -->
						<div class="form-group">
							<label for="first-name" class="col-md-2 control-label">First Name</label>
							<div class="col-md-4">
								<input type="text" class="form-control" id="first-name" name="first-name" placeholder="First Name" maxlength="<?PHP print(MAX_LENGTH_NAME); ?>" required="true" autofocus="true">
							</div>
						</div>
						<!-- Violator Last Name -->
						<div class="form-group">
							<label for="last-name" class="col-md-2 control-label">Last Name</label>
							<div class="col-md-4">
								<input type="text" class="form-control" id="last-name" name="last-name" placeholder="Last Name" maxlength="<?PHP print(MAX_LENGTH_NAME); ?>" required="true" autofocus="true">
							</div>
						</div>
						<!-- Violator Department -->
						<div class="form-group">
							<label for="department" class="col-md-2 control-label">Department</label>
							<div class="col-md-4">
								<input type="text" class="form-control" id="department" name="department" placeholder="Department" maxlength="<?PHP print(MAX_LENGTH_DEPARTMENT); ?>" required="true" autofocus="true">
							</div>
						</div>
						<!-- Date -->
						<div class="form-group">
							<label for="date" class="col-md-2 control-label">Date</label>
							<div class="col-md-4">
								<input type="date" class="form-control" id="date" name="date" required="true">
							</div>
						</div>
						<!-- Time -->
						<div class="form-group">
							<label for="time" class="col-md-2 control-label">Time</label>
							<div class="col-md-4">
								<input type="text" class="form-control" id="time" name="time" placeholder="e.g. 14:32" maxlength="<?PHP print(MAX_LENGTH_TIME_STRING); ?>" required="true">
							</div>
						</div>
						<!-- Description -->
						<div class="form-group">
							<label for="description" class="col-md-2 control-label">Description of Issue</label>
							<div class="col-md-4">
								<textarea type="text" class="form-control" id="description" name="description" maxlength="<?PHP print(MAX_LENGTH_DESCRIPTION); ?>" style="max-width: 200%; max-height: 250px;" required="true"></textarea>
							</div>
						</div>
						<!-- Buttons -->
						<div class="form-group">
							<div class="btn-group col-md-offset-2 col-md-3" role="group" aria-label="...">
								<button type="submit" class="btn btn-lg btn-default" id="submit-button">Submit Citation</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- Logout Confirmation Modal -->
	<?PHP PrintLogOutModal(); ?>
</body>
</html>