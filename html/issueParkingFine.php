<?PHP

//Flag that this is a parent file.
//Enabling included files to run.
define("CanRun", 1);

//Load external scripts.
require './phpclasses/db.php';
require './phpclasses/session.php';
require './phpclasses/dbhelper.php';
require './phpclasses/utility.php';
require './phpclasses/navbar.php';
require './phpclasses/constants.php';

//Create database and session manager objects.
$db = new DatabaseConnector();
$session = new Session("UserSession");

//Check to see if this session has expired.
if (!$session->IsAuthed())
{
	$session->HardDelete();
	header("Location: /");
	exit;
}

//Get user details and ensure the session has not been forged...
if (!CheckLogin($db, $session))
{
	//Session invalid, redirect to the login page.
	$session->HardDelete();
	header("Location: /");
	exit;
}

//Get user personal details so we can customise page contents.
$userID = $session->Get("user-id");
$userDetails = GetUserDetails($db, $userID);

//Redirect if we don't have clearance.
if ($userDetails['clearance'] == 3 || $userDetails['clearance'] == 1)
{
	header("Location: /403.php");
	exit;
}

//Declare error variables so we can
//give feedback to the user.
$showError = false;
$errorText = "";

$submitted = !empty($_POST);

if ($submitted)
{
	$showError = !IssueFine($db, $errorText);
}

function IssueFine(&$db, &$errorText)
{	
	//Get the values submitted by the form.
	$date = DBSafeText($_POST['date']);
	$time = DBSafeText($_POST['time']);
	$permitNumber = DBSafeText($_POST['permit-number']);
	$vehicleReg = strtoupper(DBSafeText($_POST['vehicle-reg']));
	$vehicleType = DBSafeText($_POST['vehicle-type']);
	$permitNumberKnown = DBSafeText($_POST['permit-number-known']);
	$paid = 0;
	
	if (ValidateFineValues($errorText, $date, $time, $permitNumber, $vehicleReg, $vehicleType, $permitNumberKnown) == false)
	{
		return false;
	}
	
	if ($permitNumberKnown == "on")
	{
		//Get vehicle registration and type from the database's permit table.		
		$result = $db->Fetch("ifb299.permits", "vehicleReg, vehicleType", "WHERE id=".$permitNumber);
		if ($result !== false && $result->num_rows > 0)
		{	
			$row = $result->fetch_assoc();
			
			$vehicleReg = $row['vehicleReg'];
			$vehicleType = $row['vehicleType'];
			
			$result->free();
		}
		else
		{
			$errorText = "Invalid permit number: Permit could not be found.";
			return false;
		}		
	}
			
	//Generate an array of field names and values.
	$fields = ["date", "time", "vehicleReg", "vehicleType", "paid"];
	$values = [$date, $time, $vehicleReg, $vehicleType, $paid];
	
	//Generate a INSERT query and run it.
	$result = $db->Insert("parkingfines", $fields, $values);
	
	return true;
}

function ValidateFineValues(&$errorText, $date, $time, $permitNumber, $vehicleReg, $vehicleType, $permitNumberKnown)
{	
	//Permit Number Known.
	if (!empty($permitNumberKnown) && $permitNumberKnown != "on")
	{
		$errorText = "Invalid permit number known checkbox value.";
		return false;
	}

	if ($permitNumberKnown == "on")
	{	
		//Permit number.
		if (!is_numeric($permitNumber))
		{
			$errorText = "Invalid permit number, must be a number.";
			return false;
		}
	}
	else
	{		
		//Vehicle reg.
		if (empty($vehicleReg) || strlen($vehicleReg) > MAX_LENGTH_VEHICLE_REG)
		{
			$errorText = "Invalid vehicle registration.";
			return false;
		}
		
		//Vehicle type.
		if (empty($vehicleType) || strlen($vehicleType) > MAX_LENGTH_VEHICLE_TYPE)
		{
			$errorText = "Invalid vehicle type.";
			return false;
		}
	}
	
	//Date.
	if (preg_match('/[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}/', $date) === 0)
	{
		$errorText = "Invalid date format.";
		return false;
	}
		
	//Time.
	if (preg_match('/[0-9]{1,2}:[0-9]{1,2}/', $time) === 0)
	{
		$errorText = "Invalid time format.";
		return false;
	}
	
	$time = explode(":", $time);
	if ($time[0] > 24 || $time[1] > 60)
	{
		$errorText = "Invalid time hour or minute values.";
		return false;
	}
	
	return true;
}

//We're done with the database connection object
//so now we delete it.
unset($db);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <link rel="icon" type="image/png" href="images/favicon.ico"/>
    <title>PH&S: Issue Parking Fine</title>
    <!-- Bootstrap core scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
	<!-- Page specific scripts -->
	<script src="js/navbar.js"></script>
	<script src="js/issueparkingfines.js"></script>
    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet"/>
	<!-- Page specific CSS -->
    <link href="/css/sidebar.css" rel="stylesheet"/>
</head>
<body>
	<div id="wrapper">
		<?PHP PrintNavBar($userDetails); ?>
		<div id="page-content-wrapper" class="container-fluid">
			<div class="container">
				<!-- Navigation Toggle Button -->		
				<a href="#menu-toggle" class="btn btn-default" id="menu-toggle"><span class="glyphicon glyphicon-menu-hamburger"></span><span> Hide Navigation</span></a>
				<!-- Jumbotron -->
				<div>
					<div class="jumbotron">
						<h1>Issue Parking Fine</h1>
						<p>Please enter the details for the parking fine below...</p>
					</div>
<?PHP				
if ($submitted)
{
	if ($showError == true)
	{
		//Report an error.
		print("<div class=\"alert alert-danger\" role=\"alert\"><b>Error:</b> ".$errorText."</div>");
	}
	else
	{	
		//Report successful submission.
		print("<div class=\"alert alert-success\" role=\"alert\"><b>Success:</b> The parking fine has been submitted!</div>");
	}
}
?>
					<!-- Parking fine form -->
					<form class="form-horizontal" action="issueparkingfine.php" method="post">
						<!-- DATE -->
						<div class="form-group">
							<label for="date" class="col-md-2 control-label">Date</label>
							<div class="col-md-4">
								<input type="date" class="form-control" id="date" name="date" required="true">
							</div>
						</div>
						<!-- TIME -->
						<div class="form-group">
							<label for="time" class="col-md-2 control-label">Time</label>
							<div class="col-md-4">
								<input type="text" class="form-control" id="time" name="time" placeholder="e.g. 14:32" maxlength="<?PHP print(MAX_LENGTH_TIME_STRING); ?>" required="true">
							</div>
						</div>
						<!-- Permit Number -->
						<div class="form-group">
							<label for="permit-number" class="col-md-2 control-label">Permit Number</label>
							<div class="col-md-4">
								<!-- Permit Number Known Checkbox -->
								<div class="checkbox">
									<label>
										<input type="checkbox" id="permit-number-known" name="permit-number-known">
										<span> Permit Number Known</span>
									</label>
								</div>
								<input type="text" class="form-control" id="permit-number" name="permit-number" placeholder="e.g. 1423" disabled="disabled" required="true">
							</div>
						</div>
						<!-- VEHICLE TYPE -->
						<div class="form-group">
							<label for="vehicle-type" class="col-md-2 control-label">Vehicle Type</label>
							<div class="col-md-4">
								<select class="form-control" id="vehicle-type" name="vehicle-type">
									<option>2-Wheel</option>
									<option>4-Wheel</option>
									<option>Other</option>
								</select>
							</div>
						</div>
						<!-- VEHICLE REGISTRATION -->
						<div class="form-group">
							<label for="vehicle-reg" class="col-md-2 control-label">Vehicle Registration</label>
							<div class="col-md-4">
								<input type="text" class="form-control" id="vehicle-reg" name="vehicle-reg" placeholder="e.g. 084GFX" maxlength="<?PHP print(MAX_LENGTH_VEHICLE_REG); ?>" required="true">
							</div>
						</div>
						<!-- Buttons -->
						<div class="form-group">
							<div class="btn-group col-md-offset-2 col-md-3" role="group" aria-label="...">
								<button type="submit" class="btn btn-md btn-default" id="submit-button">Submit Fine</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- Logout Confirmation Modal -->
	<?PHP PrintLogOutModal(); ?>
</body>
</html>