<?PHP
//Flag that this is a parent file.
//Enabling included files to run.
define('CanRun', 1);

//Load external scripts.
require './phpclasses/db.php';
require './phpclasses/session.php';
require './phpclasses/dbhelper.php';
require './phpclasses/navbar.php';

//Create database and session manager objects.
$db = new DatabaseConnector();
$session = new Session("UserSession");

//Check to see if this session has expired.
if (!$session->IsAuthed())
{
	//Session expired, redirect to the login page.
	$session->HardDelete();
	header("Location: /");
	exit;
}

//Get user details and ensure the session has not been forged...
if (!CheckLogin($db, $session))
{
	//Session invalid, redirect to the login page.
	$session->HardDelete();
	header("Location: /");
	exit;
}

//Get user personal details so we can customise page contents.
$userID = $session->Get("user-id");
$userDetails = GetUserDetails($db, $userID);

//Get current date and time.
//Update the permit status for permits that expired today.
$timeZone = new DateTimeZone("Australia/Brisbane");
$currentDate = new DateTime("now", $timeZone);
$currentDate = $currentDate->format('Y-m-d H:i:s');
$db->Update("permits", ['permitStatus'], ['Expired'], "WHERE endDate < '$currentDate'");

$result = $db->Fetch("ifb299.permits", "", "WHERE firstName='".$userDetails['firstName']."' AND lastName='".$userDetails['lastName']."'");
if ($result !== false)
{
	//Table Values
	$tableStart = "<table id=\"user-permit-table\" class=\"table\"><tbody>";
	$tableHeading = AddHeadingsToTable();
	$expiredTableHeading = AddHeadingsToTable(true);
	$tableEnd = "</tbody></table>";
	$tableOutput = "";
	
	$permitDetails = $result->fetch_assoc();
	$permitStatus = $permitDetails['permitStatus'];
	$permitExpired = false;
		
	if ($permitStatus == "Current" || $permitStatus == "Pending")
	{
		AddRowToTable($tableOutput, $permitDetails);
	}
	else if ($permitStatus == "Expired")
	{
		AddRowToTable($tableOutput, $permitDetails, true);
		$permitExpired = true;
	}
	
	$result->free();
}
else
{
	//This user has no permit.
	$permitDetails = NULL;
}

//We're done with the database connection and result objects
//so now were delete them.
unset($db);

function AddHeadingsToTable($expiredCol = false)
{
	$result = "";
	
	$result .= "<tr><th>ID</th>";
	$result .= "<th>First Name</th>";
	$result .= "<th>Last Name</th>";
	$result .= "<th>Date of Birth</th>";
	$result .= "<th>Department</th>";
	$result .= "<th>Vehicle Type</th>";
	$result .= "<th>Vehicle Registration</th>";
	$result .= "<th>Start Date</th>";
	$result .= "<th>End Date</th>";
	
	if (!$expiredCol)
	{
		$result .= "<th>Duration Type</th></tr>";
	}
	else
	{
		$result .= "<th>Duration Type</th>";
		$result .= "<th>Renew</th></tr>";
	}
	
	return $result;
}

function AddRowToTable(&$outputString, $row, $expiredCol = false)
{	
	$id = $row['id'];
	$firstName = $row['firstName'];
	$lastName = $row['lastName'];
	$dob = $row['dob'];
	$department = $row['department'];
	$vehicleType = $row['vehicleType'];
	$vehicleReg = $row['vehicleReg'];
	$startDate = $row['startDate'];
	$endDate = $row['endDate'];
	$durationType = $row['durationType'];

	$outputString .= "<tr><td>".$id."</td>";
	$outputString .= "<td>".$firstName."</td>";
	$outputString .= "<td>".$lastName."</td>";
	$outputString .= "<td>".$dob."</td>";
	$outputString .= "<td>".$department."</td>";
	$outputString .= "<td>".$vehicleType."</td>";
	$outputString .= "<td>".$vehicleReg."</td>";
	$outputString .= "<td>".$startDate."</td>";
	$outputString .= "<td>".$endDate."</td>";
	$outputString .= "<td>".$durationType."</td>";
	
	if ($expiredCol)
	{
		$outputString .= "<td><button class=\"btn btn-default\" onclick=\"Renew($id, $durationType)\"><span class=\"glyphicon glyphicon-refresh\"></span></button>";
	}
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="" />
	<meta name="author" content="" />
	<link rel="icon" type="image/png" href="images/favicon.ico" />
	<title>QUT PH&S: Virtual</title>
	<!-- Bootstrap core scripts -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<!-- Page specific scripts -->
	<script src="js/navbar.js"></script>
	<script src="js/virtual.js"></script>
	<!-- Bootstrap core CSS -->
	<link href="/css/bootstrap.min.css" rel="stylesheet" />
	<!-- Page specific CSS -->
	<link href="/css/sidebar.css" rel="stylesheet" />
</head>
<body>
	<div id="wrapper">
		<?PHP PrintNavBar($userDetails); ?>
		<div id="page-content-wrapper" class="container-fluid">
			<!-- Container -->
			<div class="container">
				<!-- Navigation Toggle Button -->
				<a href="#menu-toggle" class="btn btn-default" id="menu-toggle"><span class="glyphicon glyphicon-menu-hamburger"></span><span> Hide Navigation</span></a>
				<!-- Jumbotron -->
				<div class="jumbotron">
					<h1>Staff Virtual</h1>
					<?PHP print('<p>Welcome '.$userDetails['firstName'].' '.$userDetails['lastName'].'! Manage your permit and account details here.</p>'); ?>
				</div>
				<!-- Container Body -->
				<div>
					<div>
						<!-- Permit Details -->
						<h3>Your Permit Details:</h3>
<?PHP
//If the user has a permit...
if ($permitDetails != NULL)
{
	//Check if Permit Expired
	if ($permitExpired)
	{
		//Expired: Display alert and print expired table
		print("<div id=\"virtual-error\" class=\"alert alert-warning\" role=\"alert\"><b>Warning: </b>Your permit has expired.</div>");
		print($tableStart.$expiredTableHeading.$tableOutput.$tableEnd);
	}
	else
	{
		//Not expired: Print current table
		print($tableStart.$tableHeading.$tableOutput.$tableEnd);
	}
	
	if ($permitDetails['vehicleType'] == '2-Wheel')
	{
		print("<div><h3>Permit Rules:</h3>");
		print("<ol><li>As a driver of a two wheel drive vehicle you are required to affix your permit in the front of the vehicle.</li></ol></div>");	
	}
	else if ($permitDetails['vehicleType'] == '4-Wheel')
	{
		print("<div><h3>Permit Rules:</h3>");
		print("<ol><li>As a driver of a four wheel drive vehicle you are required to affix your permit to your vehicle windshield in the lower corner on the driver side.</li></ol></div>");
	}
}
else
{
	print("<div class=\"alert alert-warning\" role=\"alert\"><b>Warning: </b>You currently have no permit.</div>");
}
?>
					</div>
					<div>
						<!-- Account Details -->
						<h3>Account Details:</h3>
						<ul class="list-group">
<?PHP
//Print user department.
print("<li class=\"list-group-item\"><strong>Department: </strong>".$userDetails['department']."</li>");
?>
							<li class="list-group-item"><strong>Clearance Level: </strong><?PHP print($userDetails['clearance']); ?></li>
							<li class="list-group-item"><strong>You have access to: </strong>
								<ul>
									<li>Parking Permit Application</li>
									<li>Issue Citation</li>
<?PHP
switch($userDetails['clearance'])
{
	case 1: //Basic user.
	break;
	
	case 2: //Parking Inspector
		print("<li>Issue Parking Fine</li>	
			<li>Issue Smoking Fine</li>
			<li>View Permits</li>");
		break;
	case 3: //Fines Employee
		print("<li>View Permits</li>
			<li>View Citations</li>
			<li>View Parking Fines</li>
			<li>View Smoking Fines</li>");
		break;
	case 4: //Testing
		print("<li>Issue Parking Fine</li>	
			<li>Issue Smoking Fine</li>
			<li>View Permits</li>
			<li>View Citations</li>
			<li>View Parking Fines</li>
			<li>View Smoking Fines</li>");
		break;
}
?>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Logout Confirmation Modal -->
	<?PHP PrintLogOutModal(); ?>
</body>
</html>