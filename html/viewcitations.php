<?PHP
//Flag that this is a parent file.
//Enabling included files to run.
define('CanRun', 1);

//Load external scripts.
require './phpclasses/db.php';
require './phpclasses/session.php';
require './phpclasses/dbhelper.php';
require './phpclasses/navbar.php';

//Create database and session manager objects.
$db = new DatabaseConnector();
$session = new Session("UserSession");

//Check to see if this session has expired.
if (!$session->IsAuthed())
{
	//Session expired, redirect to the login page.
	$session->HardDelete();
	header("Location: /");
	exit;
}

//Get user details and ensure the session has not been forged...
if (!CheckLogin($db, $session))
{
	//Session invalid, redirect to the login page.
	$session->HardDelete();
	header("Location: /");
	exit;
}

//Get user personal details so we can customise page contents.
$userID = $session->Get("user-id");
$userDetails = GetUserDetails($db, $userID);

//Redirect if we don't have clearance.
if ($userDetails['clearance'] < 3)
{
	header("Location: /403.php");
	exit;
}

$result = $db->Fetch("ifb299.citations", "", "");
if ($result !== false)
{
	//Declare table start, headings and endings.
	$tableStart = "<table id=\"citations-table\" class=\"table\"><tbody>";
	$unresolvedTableStart = "<table id=\"unresolved-table\" class=\"table\"><tbody>";
	
	$tableHeading .= AddHeadingsToTable(false);
	$unresolvedTableHeading .= AddHeadingsToTable(true);
	
	$tableEnd = "</tbody></table>";
	
	//Declare table contents holders.
	$tableOutput = "";
	$unresolvedTableOutput = "";
	
	//Add citations to tables.
	while ($row = $result->fetch_assoc())
	{		
		$isResolved = $row['isResolved'];
		
		if ($isResolved == true)
		{
			//Citations row.
			AddRowToTable($tableOutput, $row);
		}
		else
		{
			//Unresolved citations row.
			AddRowToTable($unresolvedTableOutput, $row, true);
		}
	}
	
	$result->free();
}

//We're done with the database connection and result objects
//so now were delete them.
unset($db);

function AddHeadingsToTable($unresolved)
{
	$result = "<tr><th>ID</th>";
	$result .= "<th>First Name</th>";
	$result .= "<th>Last Name</th>";
	$result .= "<th>Department</th>";
	$result .= "<th>Date</th>";
	$result .= "<th>Time</th>";
	
	if ($unresolved)
	{
		$result .= "<th>Description</th>";
		$result .= "<th>Resolve</th></tr>";
	}
	else
	{
		$result .= "<th>Description</th>";
		$result .= "<th>Resolution Date</th>";
		$result .= "<th>Resolution Description</th></tr>";
	}
	
	return $result;
}

function AddRowToTable(&$outputString, $row, $unresolved = false)
{	
	$citationID = $row['id'];
	$firstName = $row['firstName'];
	$lastName = $row['lastName'];
	$department = $row['department'];
	$date = $row['date'];
	$time = $row['time'];
	$description = $row['description'];
	$resolutionDate = $row['resolutionDate'];
	$resolutionDescription = $row['resolutionDescription'];

	$outputString .= "<tr><td>".$citationID."</td>";
	$outputString .= "<td>".$firstName."</td>";
	$outputString .= "<td>".$lastName."</td>";
	$outputString .= "<td>".$department."</td>";
	$outputString .= "<td>".$date."</td>";
	$outputString .= "<td>".$time."</td>";
	
	if ($unresolved)
	{
		$outputString .= "<td>".$description."</td>";
		$outputString .= "<td><button class=\"btn btn-default\" onclick=\"OpenResolveModal(this, $citationID)\" ><span class=\"glyphicon glyphicon-edit\"></span></button></tr>";
	}
	else
	{
		$outputString .= "<td>".$description."</td>";
		$outputString .= "<td>".$resolutionDate."</td>";
		$outputString .= "<td>".$resolutionDescription."</td></tr>";
	}
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="" />
	<meta name="author" content="" />
	<link rel="icon" type="image/png" href="images/favicon.ico" />
	<title>PH&S: View Citations</title>
	<!-- Bootstrap core scripts -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<!-- Page specific scripts -->
	<script src="js/navbar.js"></script>
	<script src="js/viewcitations.js"></script>
	<!-- Bootstrap core CSS -->
	<link href="/css/bootstrap.min.css" rel="stylesheet" />
	<!-- Page specific CSS -->
	<link href="/css/sidebar.css" rel="stylesheet" />
	<link href="/css/viewpages.css" rel="stylesheet" />
</head>
<body>
	<div id="wrapper">
		<?PHP PrintNavBar($userDetails); ?>
		<div id="page-content-wrapper" class="container-fluid">
			<div class="container">
				<!-- Navigation Toggle Button -->
				<a href="#menu-toggle" class="btn btn-default" id="menu-toggle"><span class="glyphicon glyphicon-menu-hamburger"></span><span> Hide Navigation</span></a>
				<!-- Jumbotron -->
				<div class="jumbotron">
					<h1>Citation Search</h1>
					<p>Please enter search options below.</p>
					<!-- Citation Search Form -->
					<form id="search-form" class="form-horizontal">
						<div class="form-group">
							<!-- Search Input and Button -->
							<div class="col-sm-10">
								<div class="input-group">
									<label class="sr-only" for="search-value">Search Value</label>
									<input type="text" class="form-control" aria-label="Text input" id="search-value" name="search-value" placeholder="Enter search value and press enter to submit...">
									<div class="input-group-btn" role="group" aria-label="...">
										<button type="submit" class="btn btn-default" id="submit-button">Search</button>
									</div>
								</div>
							</div>
							<!-- Search By Type -->
							<div class="col-sm-2">
								<label class="sr-only" for="search-type">Search Type</label>
								<select class="col-sm-2 form-control" id="search-type" name="search-type">
									<option value="-1">Search By...</option>
									<option value="1">ID</option>
									<option value="2">First Name</option>
									<option value="3">Last Name</option>
									<option value="4">Department</option>
									<option value="5">Date</option>
									<option value="6">Time</option>
									<option value="7">Description</option>
									<option value="8">Resolution Date</option>
									<option value="9">Resolution Description</option>
									<option value="10">Resolved</option>
								</select>
							</div>
						</div>
						<!-- Table Error -->
						<div class="form-group" id="search-table-error">
							<div class="col-sm-12">
								<div class="alert alert-warning" role="alert"><span><b>Error: </b></span><span></span></div>
							</div>
						</div>
						<!-- Result Table -->
						<div class="form-group" id="search-table-section">
							<div class="col-sm-12">
								<table class="table" id="search-table"></table>
							</div>
						</div>
					</form>
				</div>
				<!-- Citations Panels -->
				<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
					<?PHP if (strlen($unresolvedTableOutput) > 0): ?>
					<!-- Unresolved Citations Panel -->
					<div class="panel panel-default panel-warning">
						<div class="panel-heading" role="tab" id="headingOne">
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse" data-parent="#accordion" 
								href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
									Unresolved Citations
								</a>
							</h4>
						</div>
						<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
							<div class="panel-body" id="unpaid-table-section">
								<?PHP print($unresolvedTableStart.$unresolvedTableHeading.$unresolvedTableOutput.$tableEnd); ?>
							</div>
						</div>
					</div>
					<?PHP else: ?>
					<div class="alert alert-success" role="alert"><b>There are no unresolved citations.</b></div>
					<?PHP endif; ?>
					<!-- Citations Panel -->
					<div class="panel panel-default panel-info">
						<div class="panel-heading" role="tab" id="headingTwo">
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse" data-parent="#accordion" 
								href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
									Citations
								</a>
							</h4>
						</div>
						<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
							<div class="panel-body" id="unpaid-table-section">
								<?PHP print($tableStart.$tableHeading.$tableOutput.$tableEnd); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Resolve Modal -->
	<div class="modal fade" id="resolveModal" tabindex="-1" role="dialog" aria-labelledby="resolveModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
					<h4 class="modal-title" id="myModalLabel">Resolve Citation</h4>
				</div>
				<!-- Resolution Section -->
				<div id="resolution-section" class="modal-body">
					<form class="form-horizontal" method="post">
						<input type="hidden" id="resolve-id" name="resolve-id" value="-1" />
						<table id="modal-table" class="table table-bordered">
							<tbody>
								<tr>
									<th>ID</th>
									<th>First Name</th>
									<th>Last Name</th>
									<th>Department</th>
									<th>Date</th>
									<th>Time</th>
									<th>Description</th>
								</tr>
							</tbody>
						</table>
						<!-- Resolution Description -->
						<div class="form-group">
							<label for="resolutionDescription" class="col-md-2 control-label">Resolution Description</label>
							<div class="col-md-10">
								<textarea type="text" class="form-control" id="resolution-description" name="resolutionDescription" placeholder="Description" maxlength="<?PHP print(MAX_LENGTH_DESCRIPTION); ?>" required="true"></textarea>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-primary" id="resolve-submit">Resolve Citation</button>
				</div>
			</div>
		</div>
	</div>
	<!-- Logout Confirmation Modal -->
	<?PHP PrintLogOutModal(); ?>
</body>
</html>