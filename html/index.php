<?PHP
//Flag that this is a parent file.
//Enabling included files to run.
define('CanRun', 1);

//Load external scripts.
require './phpclasses/db.php';
require './phpclasses/session.php';
require './phpclasses/utility.php';
require './phpclasses/constants.php';

//Create database and session manager objects.
$db = new DatabaseConnector();
$session = new Session("UserSession");

//Declare error variables so we can
//give feedback to the user.
$showError = false;
$errorText = "";

//Handle staff member login attempts.
$loginAttempted = isset($_POST['csrf-token']);
if ($loginAttempted)
{
	$showError = !Login($db, $session, $errorText);
}

function Login(&$db, &$session, &$errorText)
{
	//Ensure CSRF tokens match.
	if (!$session->TokensMatch($_POST['csrf-token']))
	{
		$errorText = "CSRF tokens did not match.";
		return false;
	}
	
	//Check format and length of email.
	$email = DBSafeText($_POST['email']);
	if (empty($email) || strlen($email) > MAX_LENGTH_LOGIN_EMAIL ||
		preg_match('/[A-Za-z0-9]+@[A-Za-z0-9]+.[A-Za-z]+/', $email) === 0)
	{
		$errorText = "Invalid email address given.";
		return false;
	}	
	
	//Check length of password.
	$password = DBSafeText($_POST['password']);
	if (empty($password) || strlen($password) > MAX_LENGTH_LOGIN_PASS)
	{
		$errorText = "Password length exceeds maximum.";
		return false;
	}

	//Generate a password based on input
	$tempPassword = GetPass($email, $password);
	
	//Check against database.
	$result = $db->Fetch("users", "id, login, email", "WHERE login='".$tempPassword."'");
	$row = $result->fetch_assoc();
	
	if ($result->num_rows == 0 || empty($tempPassword))
	{
		$errorText = "Incorrect login details.";
		return false;
	}
	
	if ($row['login'] == $tempPassword)
	{	
		//Handle finding a matching login...
	
		//Store user id, a hash, and session expiry in session object.
		$session->Assign("expire-time", time() + 86400);
		$session->Assign("user-id", $row['id']);
		$session->Assign("user-hash", sha1($row['id']."oldboy".$row['email']));
		
		//Redirect to staff virtual.
		header("Location: /virtual.php");
		exit;
	}
	else
	{
		$errorText = "Incorrect login details.";
		return false;
	}
	
	return true;
}

function ValidatePermitValues(&$errorText, $firstName, $lastName, $dob, $department, $vehicleType, $vehicleReg, $startTime, $permitDuration)
{
	//First name.
	if (empty($firstName) || strlen($firstName) > MAX_LENGTH_NAME ||
		preg_match('/[0-9]+/', $firstName) === 1)
	{
		$errorText = "First name value is invalid.";
		return false;
	}
	
	//Last name.
	if (empty($lastName) || strlen($lastName) > MAX_LENGTH_NAME ||
		preg_match('/[0-9]+/', $lastName) === 1)
	{	
		$errorText = "Last name value is invalid.";	
		return false;
	}
	
	//DOB.
	if (preg_match('/[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}/', $dob) === 0)
	{
		$errorText = "Date of birth value is not in the correct format.";	
		return false;
	}
	
	//Department.
	if (empty($department) || strlen($department) > MAX_LENGTH_DEPARTMENT ||
		preg_match('/[0-9]+/', $department) === 1)
	{
		$errorText = "Department value contains a number is of invalid length.";	
		return false;
	}
	
	//Vehicle type.
	if (empty($vehicleType) || strlen($vehicleType) > MAX_LENGTH_VEHICLE_TYPE)
	{
		$errorText = "Vehicle type value length is invalid.";	
		return false;
	}
	
	//Vehicle registration.
	if (empty($vehicleReg) || strlen($vehicleReg) > MAX_LENGTH_VEHICLE_REG)
	{
		$errorText = "Vehicle registration value length is invalid.";
		return false;
	}
	
	//Permit Duration.
	if (!is_numeric($permitDuration))
	{
		$errorText = "Permit duration option must be a digit.";
		return false;
	}
	
	//If this submission is for an hourly pass...
	if ($permitDuration == 1)
	{
		//Check start time input.
		if (preg_match('/[0-9]{1,2}:[0-9]{1,2}/', $startTime) === 0)
		{
			$errorText = "Permit start time is not in the correct format.";
			return false;
		}
		
		$startTime = explode(":", $startTime);
		if ($startTime[0] > 24 || $startTime[1] > 60)
		{
			$errorText = "Permit start time has invalid hour or minute values.";
			return false;
		}
	}
	
	return true;
}

//Handle guest parking permit form submissions.
$submitted = isset($_POST['first-name']) && !$loginAttempted;
if ($submitted)
{
	$showError = !GuestPermitSubmit($db, $session, $errorText);
}

function GuestPermitSubmit(&$db, &$session, &$errorText)
{
	//Get the values submitted by the form
	$firstName = trim($_POST['first-name']);	
	$lastName = trim($_POST['last-name']);
	$dob = trim($_POST['date-of-birth']);
	$department = trim($_POST['department']);
	$vehicleType = trim($_POST['vehicle-type']);
	$vehicleReg = strtoupper(trim($_POST['vehicle-registration']));
	$startTime = trim($_POST['start-time']);
	$permitDuration = trim($_POST['permit-duration']);
	
	//Validate form values.
	if (!ValidatePermitValues($errorText, $firstName, $lastName, $dob, $department, $vehicleType, $vehicleReg, $startTime, $permitDuration))
	{
		return false;
	}		
	
	$startTime = explode(":", $_POST['start-time']);
	
	//Handle the permit's duration
	switch ($permitDuration)
	{
		//Hour length permit:
		case 1:
			$date = new DateTime($_POST['start-date']);
			$date->setTime($startTime[0], $startTime[1]);
			$startDate = $date->format('Y-m-d H:i:s');
			$date->add(new DateInterval('PT1H'));
			$endDate = $date->format('Y-m-d H:i:s');
		break;
		
		//Day length permit:
		case 2: 
			$date = new DateTime($_POST['start-date']);		
			$startDate = $date->format('Y-m-d H:i:s');
			$date->add(new DateInterval('P1D'));
			$endDate = $date->format('Y-m-d H:i:s');
		break;
		
		//Show an error.
		default:
			$errorText = "Permit duration index is out of bounds.";
			return false;
		break;
	}
	
	//Generate a INSERT query.
	$fields = ["firstName", "lastName", "dob", "department", "vehicleType", "vehicleReg", "startDate", "endDate", "permitStatus", "durationType"];
	$values = [$firstName, $lastName, $dob, $department, $vehicleType, $vehicleReg, $startDate, $endDate, "Pending", $permitDuration];
	
	//Insert this permit into the permit table in the database.
	$result = $db->Insert("permits", $fields, $values);
	
	return true;
}

//We're done with the database connection object
//so now we delete it.
unset($db);
	
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <link rel="icon" type="image/png" href="images/favicon.ico"/>
    <title>PH&S: Portal</title>
    <!-- Bootstrap core scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
	<!-- Page specific scripts -->
    <script src="js/index.js"></script>
    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet"/>
	<!-- Page specific CSS -->
	<link href="/css/index.css" rel="stylesheet"/>
</head>
<body>
	<div class="container">
		<!-- Jumbotron -->
		<div class="jumbotron">
			<h1>Atmiya College<br><small>Parking, Health & Safety Portal</small></h1>
			<p>Staff may login or guests can apply for parking permits below.</p>
<?PHP
//Give feedback to the user based on the submission.
if ($loginAttempted) 
{
	//Login error.
	print("<div class=\"alert alert-warning\" role=\"alert\"><b>Error:</b> ".$errorText."</div>");
}
else if ($submitted) 
{
	if ($showError)
	{
		//Permit application error.
		print("<div class=\"alert alert-danger\" role=\"alert\"><b>Error: </b>".$errorText."</div>");		
	}
	else
	{		
		//Permit application submitted successfully.
		print("<div class=\"alert alert-info\" role=\"alert\"><b>Notice: </b>Thank you! Your parking permit has been submitted for approval.</div>");
	}
}
?>
			<div>
				<!-- Jumbotron Buttons -->
				<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#login-modal" id="staff-login-button">Staff Login</button>
				<button type="button" class="btn btn-primary btn-lg" id="guest-app-button">Guest Permit Application</button>
			</div>
		</div>
		<!-- Guest Application Section -->
		<div id="guest-app-section">
			<h2>Guest Permit Application Form:</h2>
			<!-- Guest Application Form -->
			<form class="form-horizontal" action="index.php" method="post">
				<!-- First Name -->
				<div class="form-group">
					<label for="first-name" class="col-md-2 control-label">First Name</label>
					<div class="col-md-4">
						<input type="text" class="form-control" id="first-name" name="first-name" placeholder="First Name" maxlength="<?PHP print(MAX_LENGTH_NAME); ?>" required="true">
					</div>
				</div>
				<!-- Last Name -->
				<div class="form-group">
					<label for="last-name" class="col-md-2 control-label">Last Name</label>
					<div class="col-md-4">
						<input type="text" class="form-control" id="last-name" name="last-name" placeholder="Last Name" maxlength="<?PHP print(MAX_LENGTH_NAME); ?>" required="true">
					</div>
				</div>
				<!-- DOB -->
				<div class="form-group">
					<label for="date-of-birth" class="col-md-2 control-label">Date of Birth</label>
					<div class="col-md-4">
						<input type="date" class="form-control" id="date-of-birth" name="date-of-birth" required="true">
					</div>
				</div>
				<!-- Department -->
				<div class="form-group">
					<label for="department" class="col-md-2 control-label">Department</label>
					<div class="col-md-4">
						<input type="text" class="form-control" id="department" name="department" placeholder="E.g. Science & Engineering" maxlength="<?PHP print(MAX_LENGTH_DEPARTMENT); ?>" required="true">
					</div>
				</div>
				<!-- Vehicle Type -->
				<div class="form-group">
					<label for="vehicle-type" class="col-md-2 control-label">Vehicle Type</label>
					<div class="col-md-4">
						<select class="form-control" id="vehicle-type" name="vehicle-type">
							<option>2-Wheel</option>
							<option>4-Wheel</option>
							<option>Other</option>
						</select>
					</div>
				</div>
				<!-- Vehicle Registration -->
				<div class="form-group">
					<label for="vehicle-registration" class="col-md-2 control-label">Vehicle Registration</label>
					<div class="col-md-4">
						<input type="text" class="form-control" id="vehicle-registration" name="vehicle-registration" placeholder="E.g. 084GFX" maxlength="<?PHP print(MAX_LENGTH_VEHICLE_REG); ?>" required="true">
					</div>
				</div>
				<!-- Permit Start Date -->
				<div class="form-group">
					<label for="start-date" class="col-md-2 control-label">Start Date</label>
					<div class="col-md-4">
						<input type="date" class="form-control" id="start-date" name="start-date" required="true">
					</div>
				</div>
				<!-- Permit Start Time -->
				<div class="form-group" id="time-form">
					<label for="start-time" class="col-md-2 control-label">Time</label>
					<div class="col-md-4">
						<input type="text" class="form-control" id="start-time" name="start-time" placeholder="E.g. 14:32" maxlength="<?PHP print(MAX_LENGTH_TIME_STRING); ?>">
					</div>
				</div>
				<!-- Permit Duration -->
				<div class="form-group">
					<label for="permit-duration" class="col-md-2 control-label">Permit Duration</label>
					<div class="col-md-4">
						<select class="form-control" id="permit-duration" name="permit-duration">
								<option value="1">Hourly Pass</option>
								<option value="2" selected>Day Pass</option>
						</select>
					</div>
				</div>
				<!-- Submit Button -->
				<div class="form-group">
					<div class="btn-group col-md-offset-2 col-md-3" role="group" aria-label="...">
						<button type="submit" class="btn btn-lg btn-default" id="submit-button">Submit Application</button>
					</div>
				</div>
			</form>
		</div>
	</div> 
	<!-- Login Modal -->
	<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="login-modal-label">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<!-- Modal Heading -->
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="login-modal-label">Atmyia College - Parking, Health & Safety</h4>
				</div>
				<!-- Modal Body -->
				<div class="modal-body">        
					<form action="index.php" method="post">
						<!-- CSRF Token -->
						<input type="hidden" name="csrf-token" value="<?PHP printf($session->Get("token")); ?>" />
						<!-- Email -->
						<div class="form-group">
							<label for="email" class="sr-only">Email</label>
							<input type="email" class="form-control" id="email" name="email" placeholder="Email" maxlength="<?PHP print(MAX_LENGTH_LOGIN_EMAIL); ?>" required="true">
						</div>
						<!-- Password -->
						<div class="form-group">
							<label for="password" class="sr-only">Password</label>
							<input type="password" class="form-control" id="password" name="password" placeholder="Password" maxlength="<?PHP print(MAX_LENGTH_LOGIN_PASS); ?>" required="true">
						</div>
						<!-- Remember Details Checkbox -->
						<div class="checkbox">
							<label>
								<input type="checkbox" id="remember-me" name="remember-me">
								<span> Remember Me</span>
							</label>
						</div>
						<!-- Submit Button -->
						<button type="submit" class="btn btn-lg btn-primary btn-block">Login</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>