<?PHP

//Flag that this is a parent file.
//Enabling included files to run.
define('CanRun', 1);

//Load external scripts.
require './phpclasses/db.php';
require './phpclasses/session.php';
require './phpclasses/dbhelper.php';
require './phpclasses/utility.php';
require './phpclasses/navbar.php';
require './phpclasses/constants.php';

//Create database and session manager objects.
$db = new DatabaseConnector();
$session = new Session("UserSession");

//Check to see if this session has expired.
if (!$session->IsAuthed())
{
	//Session expired, redirect to the login page.
	$session->HardDelete();
	header("Location: /");
	exit;
}

//Get user details and ensure the session has not been forged...
if (!CheckLogin($db, $session))
{
	//Session invalid, redirect to the login page.
	$session->HardDelete();
	header("Location: /");
	exit;
}

//Declare error variables so we can
//give feedback to the user.
$showError = false;
$errorText = "";

//Get user personal details so we can customise page contents.
$userID = $session->Get("user-id");
$userDetails = GetUserDetails($db, $userID);

$permitExists = false;
$submitted = !empty($_POST);

if (!$submitted)
{
	//Check to see if this user already has a permit.
	$result = $db->Fetch("ifb299.permits", "", "WHERE firstName='".$userDetails['firstName']."' AND lastName='".$userDetails['lastName']."'");
	if ($result !== false && $result->num_rows > 0)
	{		
		$permitExists = true;
		$showError = true;
		$errorText = "You already have a permit!";
		
		$result->free();
	}
}

if (!$permitExists)
{
	if ($submitted)
	{	
		$showError = StaffPermitSubmit($db, $userDetails, $errorText);
	}
}

function StaffPermitSubmit(&$db, $userDetails, &$errorText)
{
	//Get user details
	$firstName = $userDetails['firstName'];
	$lastName = $userDetails['lastName'];	
	$dob = $userDetails['dob'];

	//Get the values submitted by the form.
	$vehicleType = trim($_POST['vehicle-type']);
	$vehicleReg = strtoupper(trim($_POST['vehicle-registration']));
	$date = trim($_POST['start-date']);
	$permitDuration = trim($_POST['permit-duration']);
	
	//Validate form values.
	if (ValidatePermitValues($errorText, $vehicleType, $vehicleReg, $date, $permitDuration) == false)
	{
		return false;
	}
	
	//Handle the permit's duration.
	switch ($permitDuration)
	{
		//Month:
		case 3: 
			$date = new DateTime($_POST['start-date']);		
			$startDate = $date->format('Y-m-d H:i:s');
			$date->add(new DateInterval('P1M'));
			$endDate = $date->format('Y-m-d H:i:s');
		break;
		
		//Year:		
		case 4: 
			$date = new DateTime($_POST['start-date']);		
			$startDate = $date->format('Y-m-d H:i:s');
			$date->add(new DateInterval('P1Y'));
			$endDate = $date->format('Y-m-d H:i:s');
		break;
		
		//Show an error.
		default:
			$errorText = "Permit duration index is out of bounds.";
			return false;
		break;
	}
	
	//Remaining details are from the database's user table.			

	//Generate an INSERT query.
	$fields = ["firstName", "lastName", "dob", "department", "vehicleType", "vehicleReg", "startDate", "endDate", "permitStatus", "durationType"];
	$values = [$userDetails['firstName'], $userDetails['lastName'], $userDetails['dob'], $userDetails['department'], $vehicleType, $vehicleReg, $startDate, $endDate, "Pending", $permitDuration];
	
	//Insert this permit into the permit table in the database.
	$result = $db->Insert("permits", $fields, $values);
}

function ValidatePermitValues(&$errorText, $vehicleType, $vehicleReg, $date, $permitDuration)
{
	//Vehicle type.
	if (empty($vehicleType) || strlen($vehicleType) > MAX_LENGTH_VEHICLE_TYPE)
	{
		$errorText = "Vehicle type value length is invalid.";
		return false;
	}
	
	//Vehicle registration.
	if (empty($vehicleReg) || strlen($vehicleReg) > MAX_LENGTH_VEHICLE_REG)
	{		
		$errorText = "Vehicle registration value length is invalid.";
		return false;
	}
	
	//Date of birth.
	if (preg_match('/[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}/', $date) === 0)
	{
		$errorText = "Date of birth value is in an incorrect format.";
		return false;
	}
	
	//Permit Duration.
	if (!is_numeric($permitDuration))
	{
		$errorText = "Permit duration value must be a digit.";
		return false;
	}
	
	return true;
}

//We're done with the database connection object
//so now we delete it.
unset($db);

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="" />
	<meta name="author" content="" />
	<link rel="icon" type="image/png" href="images/favicon.ico" />
	<title>PH&S: Staff Permit Application</title>
	<!-- Bootstrap core scripts -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<!-- Page specific scripts -->
	<script src="js/navbar.js"></script>
	<!-- Bootstrap core CSS -->
	<link href="/css/bootstrap.min.css" rel="stylesheet" />
	<!-- Page specific CSS -->
	<link href="/css/sidebar.css" rel="stylesheet" />
</head>
<body>
	<div id="wrapper">
		<?PHP PrintNavBar($userDetails); ?>
		<div id="page-content-wrapper" class="container-fluid">
			<!-- Container -->
			<div class="container">
				<!-- Navigation Toggle Button -->
				<a href="#menu-toggle" class="btn btn-default" id="menu-toggle"><span class="glyphicon glyphicon-menu-hamburger"></span><span> Hide Navigation</span></a>
				<!-- Jumbotron -->
				<div class="jumbotron">
					<h1>Permit Application</h1>
					<p>Please enter the details for your parking permit.</p>
				</div>
<?PHP
//Give feedback to the user...
if ($showError)
{
	//Errors.
	print("<div class=\"alert alert-danger\" role=\"alert\"><b>Error: </b>".$errorText."</div>");		
}
else if ($submitted)
{
	//Success!
	print("<div class=\"alert alert-success\" role=\"alert\"><b>Success:</b> Your Permit has been submitted!</div>");
}

if (!$permitExists):
?>
				<!-- Permit Form -->
				<form class="form-horizontal" action="permitapplication.php" method="post">
					<!-- Vehicle Type -->
					<div class="form-group">
						<label for="vehicle-type" class="col-md-2 control-label">Vehicle Type</label>
						<div class="col-md-4">
							<select class="form-control" id="vehicle-type" name="vehicle-type">
								<option>2-Wheel</option>
								<option>4-Wheel</option>
								<option>Other</option>
							</select>
						</div>
					</div>
					<!-- Vehicle Registration -->
					<div class="form-group">
						<label for="vehicle-registration" class="col-md-2 control-label">Vehicle Registration</label>
						<div class="col-md-4">
							<input type="text" class="form-control" id="vehicle-registration" name="vehicle-registration" placeholder="eg. 084GFX" maxlength="<?PHP print(MAX_LENGTH_VEHICLE_REG); ?>" required="true">
						</div>
					</div>
					<!-- Permit Start Date -->
					<div class="form-group">
						<label for="start-date" class="col-md-2 control-label">Start Date</label>
						<div class="col-md-4">
							<input type="date" class="form-control" id="start-date" name="start-date" required="true">
						</div>
					</div>
					<!-- Permit Duration -->
					<div class="form-group">
						<label for="permit-duration" class="col-md-2 control-label">Permit Duration</label>
						<div class="col-md-4">
							<select class="form-control" id="permit-duration" name="permit-duration">
								<option value="3">Monthly Pass</option>
								<option value="4">Yearly Pass</option>
							</select>
						</div>
					</div>
					<!-- Submit Button -->
					<div class="form-group">
						<div class="btn-group col-md-offset-2 col-md-3" role="group" aria-label="...">
							<button type="submit" class="btn btn-lg btn-default" id="submit-button">Submit Permit</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- Logout Confirmation Modal -->
<?PHP 
endif;

PrintLogOutModal();
?>
</body>
</html>